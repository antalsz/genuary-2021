#!/bin/zsh
if (( $# != 2 )); then
  echo "Usage: $0 SRC DEST" >&2
  exit 1
fi

# From "How to make GIFs with FFMPEG", by Collin Burger, at GIPHY Engineering, March 29, 2018
# <https://engineering.giphy.com/how-to-make-gifs-with-ffmpeg/>
ffmpeg -i $1 -filter_complex "[0:v] split [v1][v2]; [v1] palettegen [p]; [v2][p] paletteuse" $2
