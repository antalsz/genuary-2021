#!/bin/zsh
ag --no-file -io '(?<={-# LANGUAGE )(.+)(?= #-}$)' | sort -u | grep -v '^$'
