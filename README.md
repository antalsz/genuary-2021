# Genuary 2021 – Antal Spector-Zabusky

My art for [Genuary 2021](https://genuary2021.github.io/).  Each piece is listed with its date, its name, and its [prompt](https://genuary2021.github.io/prompts).

## January 1: *Triangles*

Prompt: `// TRIPLE NESTED LOOP`

![Overlapping rainbow triangles on a black disc](01 – Triangles.svg)

## January 2: *An Old Kind of Noise*

Prompt: [Rule 30](https://www.wolframalpha.com/input/?i=rule+30) ([elementary cellular automaton](https://en.wikipedia.org/wiki/Rule_30))

![Black tangles looping around in a circle](02 – An Old Kind of Noise.svg)

## January 3: *Outer Paean*

Prompt: Make something human.

Excerpts from [the full document](03 – Outer Paean.pdf):

> ### Outer Paean
>
> Generated poems in the generated languages Titatñu, Ñeñ, and Søkŋ̄äcø.
>
> #### Poetry
>
> ##### Titatñu
>
> Azɫabibvi çawa agwa\
> L̃wati aztitra wavigwa\
> Ztuŋtaza ibzu\
> Vidzi u ibzu\
> Vidzi talujz̧u atwagwa
>
> ##### Ñeñ
>
> Emeññam immemen neñ ememim\
> Ñimim ñimim memminmem nôn môñim
>
> \* * *
>
> Ñañmim mîñ mimeñnôñ emminônin\
> Mimnanmin ônnômmeñ nôn mômeñin
>
> \* * *
>
> Niniñ ñimim ônnômmeñ ñônnômim\
> Amiñ eñnamin mimñañ ñimñemim
>
> ##### Søkŋ̄äcø
>
> Kä'øsu kxokuçsäts kxokuçsäts tøh tuhcäŋ̄\
> Ṉ̃øsŋ̄øc kxokuçsäts säfɫ̄äɫ̄ä 'häkøfsø'sä\
> Suḻ̃tsuŋ̄øs kxokuçsäts pfä' ḻ̃ut ŋ̄u'h säkøcäŋ̄\
> Toŋ̄ kxokuçsäts kxokuçsäts tätä'suḻ̃ sussä
>
> Ɫ̄äscosä ŋ̄øḻ̃ätøcçḻ̃ä totä cøkx'uḻä\
> Häsø kxokuçsäts kxu pfä' ḻ̃oḻ̃ 'hutḻ̃øsucäŋ̄\
> Kusu totä ɫ̄äscosä säŋ̄cu køhäḻä\
> Cøŋ̄ sok susä tätusøs pfä' pfä' pføḻ̃oṉ̃cäŋ̄
>
> Totä føtɫ̄opfsäŋ̄ cçä'hä ŋ̄äs'øç käŋ̄ässä\
> Ɫ̄ätsäs ḻ̃øm̱o sätukxuŋ̄ kxokuçsäts ṉokxcäŋ̄\
> Kxokuçsäts ḻ̃ätuk ɫ̄äscosä pfä' sufussä\
> Cuŋ̄äḻ̃kxäçŋ̄u ɫ̄äscosä ŋ̄ussuc kätsøkcäŋ̄
>
> Sucäs cäŋ̄um̱soc pfä ɫ̄äsxosoḻ̃u ɫ̄äcäŋ̄\
> Tsä'ä susŋ̄us kxokuçsäts kxø'häfäḻ ḻ̃ukḻä\
> Kxokuçsäts ŋ̄øcçäŋ̄ 'huɫ̄u kxätsu koɫ̄äŋ̄cäŋ̄\
> Kusu ŋ̄äpsussäɫ̄u ḻ̃ätuk totä kxäcḻä

(This took two days.)

## January 4: *Windowpane*

Prompt: Small areas of symmetry.

![A black and white noise cloud that fades out at circular apertures to reveal a colorful grid](04 – Windowpane.png)

(This was delayed by a day.)

## January 5: *Tetraviolet*

Prompt: Do some code golf! How little code can you write to make something
interesting? Share the sketch and its code together if you can.

![An oscillating green background with four red pillars surrounded by violet halos](05 – Tetraviolet.png)

[`generateImage(\x y->let t%k=round.(*255).abs$t(f x*4*k*pi)*t(f y*pi::Float);f=(/500).fromIntegral in PixelRGB8(sin%1)(cos%1)$sin%2)500 500`](src/lib/Genuary2021/January05.hs)

(This was delayed by a day.)

## January 6: *F⊠CK TR⊠MP*

Prompt: Triangle subdivision.

![The words "FUCK TRUMP" spelled out, one above the other, in a blocky font where each letter has lines connecting all the internal vertices; "FUCK" is on the top in red on light blue, and "TRUMP" is on the bottom in blue on light red](06 – F⊠CK TR⊠MP.svg)

## January 7: *Square Sun (The Computer Was Bored)*

Prompt: Generate some rules, then follow them by hand on paper.

> 1. Draw four circles.
> 2. Draw a square in the corners of the page.
> 3. Connect the circles with loop-de-loops.
> 4. Separate the extremal points of the circles with loop-de-loops.
> 5. Connect the shapes with straight lines.

[![A photograph of a drawing on a piece of paper conducted according to the above rules: in the top right, there's a square, with lines connecting it to four circles in a horizontal line two-thirds of the way down the page.  Each circle is divided with an "X" with a cluster of four circular loops in the middle.  The circles are connected to their neighbors with loop-de-looping coiled lines, and each have straight lines sticking out the bottom connecting to a horizontal line that runs under all of them.](<07 – Square Sun (The Computer Was Bored) [small].jpg>)](<07 – Square Sun (The Computer Was Bored).jpg>)

## January 8: *Simply Shaded*

Prompt: Curve only.

![The Sierpiński curve, a space-filling curve, drawn after 4 iterations; it is colored with a rainbow gradient and on a black background.](08 – Simply Shaded.svg)

## January 9: *Interweaving*

Prompt: Interference patterns.

Click for an actual video file (better quality and >50× smaller).

[![Curving magenta lines separating regions of dark blue and dark red; the lines and regions shift, bringing the regions together infinitely.](<09 – Interweaving.gif>)](<09 – Interweaving.mp4>)

(This took roughly an extra half-day to finalize the art, and then longer dealing with video formats.)

## January 10: *Feel the Urn*

Prompt: `// TREE`

![An H tree, a tree composed of H-like lines, with varying line-widths and colors; the lines reach a common (average) width and average hue in the middle, with the color saturation increasing from gray at the edges to bright in the center.](10 – Feel the Urn.svg)

(This was delayed by four days.)

## January 11: *Dice & Diagrams*

Prompt: Use something other than a computer as an autonomous process (or use a
non-computer random source).

I chose the latter, and used dice rolls (d8s and d20s) as my random source.

![A 10×10 grid of squares outlined in light gray, with colorful shapes scattered
randomly around in the grid cells.  The shapes are circles, equilateral triangles pointing either up or down, and squares; the colors are pink, red, orange, yellow, green, light blue, dark blue, and violet.](<11 – Dice & Diagrams.svg>)

(This was delayed by four days.)
