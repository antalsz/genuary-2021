#!/bin/zsh
set -e
if [[ ! ($# -eq 1 || $# -eq 2) ]]; then
  echo "Usage: $0 SVG [PNG]"
  exit 1
fi

svg=$1
if [[ $# -eq 2 ]] then
  png=$2
elif [[ $svg == *.svg ]] then
  png=${1%.svg}.png
else
  png=$1.png
fi

inkscape --export-dpi=300 --export-png "$(realpath $png)" "$(realpath $svg)"
