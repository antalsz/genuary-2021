{-# LANGUAGE TupleSections #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module Genuary2021 where

import Control.Monad

import Text.Read

import System.IO
import System.Environment
import System.Exit

import Genuary2021.Util.Enum

import qualified Genuary2021.January01
import qualified Genuary2021.January02
import qualified Genuary2021.January03
import qualified Genuary2021.January04
import qualified Genuary2021.January05
import qualified Genuary2021.January06
import qualified Genuary2021.January07
import qualified Genuary2021.January08
import qualified Genuary2021.January09
import qualified Genuary2021.January10
import qualified Genuary2021.January11

data Day = Jan01 | Jan02 | Jan03 | Jan04 | Jan05 | Jan06 | Jan07
         | Jan08 | Jan09 | Jan10 | Jan11 | Jan12 | Jan13 | Jan14
         | Jan15 | Jan16 | Jan17 | Jan18 | Jan19 | Jan20 | Jan21
         | Jan22 | Jan23 | Jan24 | Jan25 | Jan26 | Jan27 | Jan28
         | Jan29 | Jan30 | Jan31
         deriving (Eq, Ord, Enum, Bounded, Show, Read)

readDay :: String -> Maybe Day
readDay = toEnumMaybe . subtract 1 <=< readMaybe

dayNumber :: Day -> Int
dayNumber = (+1) . fromEnum

usage :: Handle -> IO ()
usage out = do
  name <- getProgName
  hPutStrLn out $ "Usage: " ++ name ++ " DAY [ARGS...]"
  hPutStrLn out $ "       " ++ name ++ " help"

help :: Handle -> IO ()
help out = do
  usage out
  hPutStrLn out $ ""
  hPutStrLn out $ "Display the art for one day of Genuary 2021.  DAY must be between 1 and 31."

dayMain :: Day -> IO ()
dayMain Jan01 = Genuary2021.January01.main
dayMain Jan02 = Genuary2021.January02.main
dayMain Jan03 = Genuary2021.January03.main
dayMain Jan04 = Genuary2021.January04.main
dayMain Jan05 = Genuary2021.January05.main
dayMain Jan06 = Genuary2021.January06.main
dayMain Jan07 = Genuary2021.January07.main
dayMain Jan08 = Genuary2021.January08.main
dayMain Jan09 = Genuary2021.January09.main
dayMain Jan10 = Genuary2021.January10.main
dayMain Jan11 = Genuary2021.January11.main
dayMain day   = putStrLn $ "Day " ++ show (dayNumber day) ++ " not done… yet!"

runDayMain :: Day -> String -> [String] -> IO ()
runDayMain day dayStr args = do
  name <- getProgName
  withProgName (name ++ " " ++ dayStr) . withArgs args $ dayMain day

main :: IO ()
main = getArgs >>= \case
  (parseDay -> Just (day, dayStr)) : dayArgs -> runDayMain day dayStr dayArgs
  (parseHelp -> True) : _                    -> help stdout
  _                                          -> usage stderr *> exitFailure
  where
    parseDay s = (,s) <$> readDay s
    parseHelp "help"   = True
    parseHelp "--help" = True
    parseHelp "-help"  = True
    parseHelp "-h"     = True
    parseHelp "-?"     = True
    parseHelp _        = False
