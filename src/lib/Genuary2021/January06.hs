{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Genuary2021.January06 where

-- Prompt: Triangle subdivision.

import Data.Bifunctor
import Control.Monad

import Diagrams.Prelude
import Diagrams.TwoD.Path
import Diagrams.Backend.SVG.CmdLine

pathFromTrail' :: (Metric v, Floating n, Ord n) => Trail' l v n -> Path v n
pathFromTrail' = pathFromTrail . Trail

type Letter n = [[P2 n]]

letter :: [[(n,n)]] -> Letter n
letter = map $ map p2

letter' :: [(n,n)] -> Letter n
letter' = letter . pure

-- (1) Generating these letters was unpleasantly manual
-- (2) Would there have been a better way to represent these?
letterF, letterU, letterC, letterK, letterT, letterR, letterM, letterP :: Letter Double
letterF = letter' [ (0,5), (0,0), (1,0), (1,2), (2,2), (2,3), (1,3), (1,4), (3,4), (3,5) ]
letterU = letter' [ (0,0), (4,0), (4,5), (3,5), (3,1), (1,1), (1,5), (0,5) ]
letterC = letter' [ (0,0), (3,0), (3,1), (1,1), (1,4), (3,4), (3,5), (0,5) ]
letterK = letter' [ (0,0), (0,5), (1,5), (1,3), (3,5), (4,5), (2.6, 3.6), (3.85,0), (3,0), (2,3), (1,2), (1, 0)]
letterT = letter' [ (0,5), (4,5), (4,4), (2.5,4), (2.5,0), (1.5,0), (1.5,4), (0,4) ]
letterR = letter  [ [ (0,0), (0,5), (4,5), (4,2), (3,2), (4,0), (3,0), (2,2), (1,2), (1,0) ]
                  , [ (1,3), (3,3), (3,4), (1,4) ] ]
letterM = letter' [ (0,0), (1,0), (1,4), (2,2), (3,2), (4,4), (4,0), (5,0), (5,5), (3.5,5), (2.5,3), (1.5,5), (0,5) ]
letterP = letter  [ [ (0,0), (0,5), (3,5), (3,2), (1,2), (1, 0) ]
                  , [ (1,3), (2,3), (2,4), (1,4) ] ]

pointsInsideLine :: Fractional n => Int -> P2 n -> P2 n -> [P2 n]
pointsInsideLine n pt1 pt2 = [lerp (fromIntegral i/fromIntegral (n+1)) pt1 pt2 | i <- [1..n]]

outlinePath :: RealFloat n => Letter n -> Path V2 n
outlinePath = foldMap $ pathFromLocTrail . mapLoc (wrapLoop . closeLine) . fromVertices

triangulatedPaths :: RealFloat n => Letter n -> (Path V2 n, Path V2 n)
triangulatedPaths letter =
  let outline    = outlinePath letter
      innerLines = [ line | pt1 <- concat letter, pt2 <- concat letter, pt1 /= pt2
                          , let line = pt1 ~~ pt2
                          , all (isInsideWinding outline) (pointsInsideLine 100 pt1 pt2)]
  in (outline, mconcat innerLines)

type Styler b v n m = QDiagram b v n m -> QDiagram b v n m

drawLetter :: (TypeableFloat n, Renderable (Path V2 n) b)
           => Letter n -> Styler b V2 n Any -> Styler b V2 n Any -> QDiagram b V2 n Any
drawLetter letter outer inner = uncurry beneath
                              . bimap outer inner
                              . join bimap strokePath
                              . triangulatedPaths
                              $ letter

spell :: (TypeableFloat n, Renderable (Path V2 n) b)
      => [Letter n] -> Styler b V2 n Any -> Styler b V2 n Any -> QDiagram b V2 n Any
spell letters outer inner = centerX $ hsep 1 [ drawLetter l outer inner | l <- letters ]

-- OK, it's not imaginative, but I was working on this right after he tried to
-- stage a coup, so.

fuckTrump :: Renderable (Path V2 Double) b => QDiagram b V2 Double Any
fuckTrump = vsep 1 [ spellColors [letterF, letterU, letterC, letterK]          red  lightblue
                   , spellColors [letterT, letterR, letterU, letterM, letterP] blue pink ]
  where spellColors letters fg bg = spell letters (lw 1.5) (lw 1) # lc fg # fc bg

-- Fun fact: F, K, R, and P (at least) all have small non-triangular portions.
-- Oh well, it's still /inspired by/ triangular subdivision!

main :: IO ()
main = mainWith . frame 0.5 $ fuckTrump @B
