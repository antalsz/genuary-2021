{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
 
module Genuary2021.January09 where

-- Prompt: Interference patterns.

import Data.Coerce
import Genuary2021.Util.Coerce

import Data.Functor
import Control.Monad
import Control.Monad.Primitive
import Genuary2021.Util.Primitive
import Data.Function
import Genuary2021.Util.Function

import qualified Data.Vector.Unboxed         as VU
import qualified Data.Vector.Unboxed.Mutable as VUM
import qualified Data.Vector.Generic.Mutable as VGM
import qualified Data.Vector.Generic         as VG

import Data.Colour
import Genuary2021.Util.Color

import Codec.Picture
import Genuary2021.Util.Image

import Codec.FFmpeg
import Genuary2021.Util.Video

import Options.Applicative

type ℝ = Double -- Easily swap between 'Float' and 'Double' to test which is better

data WaveSource = WaveSource { wsX, wsY    :: !ℝ
                             , wsFrequency :: !ℝ
                             , wsAmplitude :: !ℝ
                             , wsShift     :: !ℝ }
                  deriving (Eq, Ord, Show, Read)

wsTuple :: WaveSource -> (ℝ,ℝ,ℝ,ℝ,ℝ)
wsTuple WaveSource{..} = (wsX,wsY,wsFrequency,wsAmplitude,wsShift)
{-# INLINE wsTuple #-}

tupleWS :: (ℝ,ℝ,ℝ,ℝ,ℝ) -> WaveSource
tupleWS (wsX,wsY,wsFrequency,wsAmplitude,wsShift) = WaveSource{..}
{-# INLINE tupleWS #-}

newtype instance VU.MVector s WaveSource = MV_WaveSource (VU.MVector s (ℝ,ℝ,ℝ,ℝ,ℝ))
newtype instance VU.Vector    WaveSource = V_WaveSource  (VU.Vector    (ℝ,ℝ,ℝ,ℝ,ℝ))

instance VGM.MVector VU.MVector WaveSource where
  basicLength                          = coerce $ VGM.basicLength @VU.MVector
  basicUnsafeSlice                     = coerce $ VGM.basicUnsafeSlice @VU.MVector
  basicOverlaps                        = coerce $ VGM.basicOverlaps @VU.MVector
  basicUnsafeNew                       = fmap MV_WaveSource . VGM.basicUnsafeNew
  basicInitialize                      = coerce1Arg $ VGM.basicInitialize @VU.MVector
  basicUnsafeReplicate k ws            = MV_WaveSource <$> VGM.basicUnsafeReplicate k (wsTuple ws)
  basicUnsafeRead (MV_WaveSource v)    = fmap tupleWS . VGM.basicUnsafeRead v
  basicUnsafeWrite (MV_WaveSource v) i = VGM.basicUnsafeWrite v i . wsTuple
  basicClear                           = coerce1Arg $ VGM.basicClear @VU.MVector
  basicSet (MV_WaveSource v)           = VGM.basicSet v . wsTuple
  basicUnsafeCopy                      = coerce2Args $ VGM.basicUnsafeCopy @VU.MVector
  basicUnsafeMove                      = coerce2Args $ VGM.basicUnsafeMove @VU.MVector
  basicUnsafeGrow (MV_WaveSource v)    = fmap MV_WaveSource . VGM.basicUnsafeGrow v
  
  {-# INLINE basicLength          #-}
  {-# INLINE basicUnsafeSlice     #-}
  {-# INLINE basicOverlaps        #-}
  {-# INLINE basicUnsafeNew       #-}
  {-# INLINE basicInitialize      #-}
  {-# INLINE basicUnsafeReplicate #-}
  {-# INLINE basicUnsafeRead      #-}
  {-# INLINE basicUnsafeWrite     #-}
  {-# INLINE basicClear           #-}
  {-# INLINE basicSet             #-}
  {-# INLINE basicUnsafeCopy      #-}
  {-# INLINE basicUnsafeMove      #-}
  {-# INLINE basicUnsafeGrow      #-}

instance VG.Vector VU.Vector WaveSource where
  basicUnsafeFreeze (MV_WaveSource v) = V_WaveSource  <$> VG.basicUnsafeFreeze v
  basicUnsafeThaw (V_WaveSource v)    = MV_WaveSource <$> VG.basicUnsafeThaw v
  basicLength                         = coerce $ VG.basicLength @VU.Vector
  basicUnsafeSlice                    = coerce $ VG.basicUnsafeSlice @VU.Vector
  basicUnsafeIndexM (V_WaveSource v)  = fmap tupleWS . VG.basicUnsafeIndexM v
  basicUnsafeCopy                     = coerce2Args $ VG.basicUnsafeCopy @VU.Vector
  elemseq _                           = seq -- Because 'WaveSource' has only strict fields
  
  {-# INLINE basicUnsafeFreeze #-}
  {-# INLINE basicUnsafeThaw   #-}
  {-# INLINE basicLength       #-}
  {-# INLINE basicUnsafeSlice  #-}
  {-# INLINE basicUnsafeIndexM #-}
  {-# INLINE basicUnsafeCopy   #-}
  {-# INLINE elemseq           #-}
  
instance VU.Unbox WaveSource

distance :: Floating a => (a,a) -> (a,a) -> a
distance (x₁,y₁) (x₂,y₂) = let dx = x₁ - x₂
                               dy = y₂ - y₁
                           in sqrt $ dx*dx + dy*dy
{-# INLINABLE distance #-}
{-# SPECIALIZE distance :: (Float,Float) -> (Float,Float) -> Float #-}
{-# SPECIALIZE distance :: (Double,Double) -> (Double,Double) -> Double #-}

type WaveSources s = VUM.MVector s WaveSource

data WaveColors = WaveColors { negativeColor, zeroColor, positiveColor :: !(Colour ℝ) }
                deriving (Eq, Show, Read)

data WaveSystem s = WaveSystem { waveSources :: !(WaveSources s)
                               , waveColors  :: !WaveColors }

intensity :: (PrimMonad m, PrimState m ~ s) => WaveSources s -> ℝ -> ℝ -> m ℝ
intensity waveSources x y = do
  let sumBy f = VU.foldl' (\acc x -> acc + f x) 0 <$> VU.unsafeFreeze waveSources -- Is this the best way?
      value WaveSource{..} = wsAmplitude * sin (wsShift + wsFrequency*(distance (wsX,wsY) (x,y))*2*pi)
  maxValue   <- sumBy $ abs . wsAmplitude
  totalValue <- sumBy value
  pure $ totalValue / maxValue

color :: WaveColors -> ℝ -> Colour ℝ
color WaveColors{..} i | i < 0     = blend (abs i) negativeColor zeroColor
                       | otherwise = blend i       positiveColor zeroColor

evaluate :: (PrimMonad m, PrimState m ~ s) => WaveSystem s -> ℝ -> ℝ -> m (Colour ℝ)
evaluate WaveSystem{..} = fmap (color waveColors) .: intensity waveSources

data AnimationParameters = AnimationParameters { output         :: !FilePath
                                               , frames         :: !Int
                                               , encodingParams :: !EncodingParams }

animateWaveSystem :: forall m. PrimIOMonad m
                  => AnimationParameters
                  -> WaveColors
                  -> [ℝ -> WaveSource]
                  -> m ()
animateWaveSystem AnimationParameters{..} waveColors animatedWaveSources = do
  waveSources <- VUM.unsafeNew $ length animatedWaveSources
  let waveSystem = WaveSystem{..}
  animateM @PixelRGB8 time01 cartesianBiunitIncl output encodingParams frames
    (\t -> zipWithM_ (VUM.unsafeWrite waveSources) [0..] $ animatedWaveSources <&> ($ t))
    (const $ evaluate waveSystem)
{-# INLINE animateWaveSystem #-}

blueToRed :: WaveColors
blueToRed = WaveColors
  { negativeColor = hsluvℝ 265.9 1 0.25 -- dark blue
  , zeroColor     = hsluvℝ 307.1 1 0.5  -- bright magenta
  , positiveColor = hsluvℝ 12.2  1 0.25 -- dark red
  }
  where hsluvℝ h s l = colourConvert $ hsluv h s l

moveToCorner, changeShifts, offCenterLoop, centeredLoop, spiral, thereAndBackAgain :: [ℝ -> WaveSource]
moveToCorner =
  [ \t -> WaveSource { wsX = -0.5+t, wsY = -0.5+t, wsFrequency = 2, wsAmplitude = 1, wsShift = 0 }
  , \_ -> WaveSource { wsX = 0.5,    wsY =  0.5,   wsFrequency = 2, wsAmplitude = 3, wsShift = 0 } ]
changeShifts =
  [ \t -> WaveSource { wsX = -0.5, wsY = -0.5, wsFrequency = 2, wsAmplitude = 1, wsShift = t*2*pi }
  , \t -> WaveSource { wsX =  0.5, wsY =  0.5, wsFrequency = 2, wsAmplitude = 1, wsShift = t*2*pi } ]
offCenterLoop =
  [ \t -> WaveSource { wsX = -0.5 + cos (t*2*pi)/1.5
                     , wsY = -0.5 + sin (t*2*pi)/1.5
                     , wsFrequency = 2
                     , wsAmplitude = 1
                     , wsShift     = 0 }
  , \_ -> WaveSource { wsX = 0, wsY = 0, wsFrequency = 2, wsAmplitude = 1, wsShift = 0 } ]
centeredLoop =
  [ \t -> WaveSource { wsX = cos (t*2*pi)/2
                     , wsY = sin (t*2*pi)/2
                     , wsFrequency = 2
                     , wsAmplitude = 1
                     , wsShift     = 0 }
  , \_ -> WaveSource { wsX = 0, wsY = 0, wsFrequency = 2, wsAmplitude = 1, wsShift = 0 } ]
spiral =
  [ \t -> WaveSource { wsX = (1-t) * cos (3*t*2*pi)
                     , wsY = (1-t) * sin (3*t*2*pi)
                     , wsFrequency = 2
                     , wsAmplitude = 1
                     , wsShift     = 0 }
  , \_ -> WaveSource { wsX = 0, wsY = 0, wsFrequency = 2, wsAmplitude = 1, wsShift = 0 } ]
thereAndBackAgain =
  [ \t -> WaveSource { wsX = 0.5 - 2*abs (t-0.5)
                     , wsY = 0.5 - 2*abs (t-0.5)
                     , wsFrequency = 2, wsAmplitude = 1, wsShift = 0 }
  , \_ -> WaveSource { wsX = 0.5, wsY = 0.5, wsFrequency = 2, wsAmplitude = 3, wsShift = 0 } ]

-- There's room to play with some really cool stuff here in the future.
--
-- 1. Basically /any/ of the components of the wave system could vary smoothly –
--    not just the wave sources, but also the /colors/.
--
-- 2. The pieces could vary together instead of separately as they do now.
--
-- 3. Instead of functions from time, we could do piecemeal updates in a
--    derivative style.
--
-- 4. Randomly generating wave sources and having them vary along paths given by
--    4D noise (see
--    <https://www.generativehut.com/post/using-noise-to-create-looping-gifs-on-processing>)
--    would allow for some cool randomized looping behavior.
--
-- Also, I could review the unsafe code I have lying around in here and try to
-- compartmentalize it better.

data Arguments = Arguments { argOutput :: !FilePath
                           , argFrames :: !Int
                           , argFPS    :: !Int
                           , argSize   :: !Int }
               deriving (Eq, Ord, Show, Read)

argumentsToParameters :: Arguments -> AnimationParameters
argumentsToParameters Arguments{..} = AnimationParameters
  { output = argOutput
  , frames = argFrames
  , encodingParams = ((defaultParams `on` fromIntegral) argSize argSize){epFps = argFPS} }

argumentsParser :: Parser Arguments
argumentsParser =
  Arguments <$> strArgument
                  (  metavar "FILE"
                  <> help "Output file" )
            <*> option auto
                  (  short 'f'
                  <> long "frames"
                  <> metavar "INT"
                  <> help "The number of frames (how long the animation is)"
                  <> value 120 <> showDefault )
            <*> option auto
                  (  short 's'
                  <> long "fps"
                  <> metavar "INT"
                  <> help "The number of frames per second (how fast the animation is)"
                  <> value 30 <> showDefault )
            <*> option auto
                  (  short 'd'
                  <> long "size"
                  <> metavar "INT"
                  <> help "The width/height of the animation in pixels (how large the animation is)"
                  <> value 500 <> showDefault )

argumentsInfo :: ParserInfo Arguments
argumentsInfo = info
  (argumentsParser <**> helper)
  (fullDesc <> progDesc "Generate a blue and red wave interference animation.  \
                        \To configure which animation, you would have to edit the code.")

main :: IO ()
main = do
  animationParameters <- argumentsToParameters <$> execParser argumentsInfo
  initFFmpeg
  animateWaveSystem animationParameters blueToRed changeShifts
