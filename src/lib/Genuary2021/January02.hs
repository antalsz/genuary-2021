{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ViewPatterns #-}

module Genuary2021.January02 where

-- Prompt: Rule 30 (elementary cellular automaton)

import Data.Bits
import Data.Word
import Data.Foldable
import Data.Bifoldable
import Genuary2021.Util.Function

import Control.Monad.State

import Diagrams.Prelude
import Diagrams.TwoD.Path.Turtle
import Diagrams.Backend.SVG.CmdLine
import Genuary2021.Util.Color

type Rule = Word8

type Cell = Bool
pattern O, I :: Bool
pattern O = False
pattern I = True
{-# COMPLETE O, I #-}

short :: [Cell] -> String
short = map \case O -> 'O'
                  I -> 'I'

pretty :: [Cell] -> String
pretty = map \case O -> ' '
                   I -> '█'

prettyAligned :: Padding -> [[Cell]] -> String
prettyAligned Padding{..} rect =
  let maxWidth = maximum $ 0 : map length rect
      pad' cells =
        let gap = maxWidth - length cells
        in replicate (gap `quot` 2) leftPadding ++ cells ++ replicate ((gap+1) `quot` 2) rightPadding
  in unlines $ map (pretty . pad') rect

asBit :: Bits a => Bool -> Int -> a
asBit True  i = bit i
asBit False _ = zeroBits

-- There's gotta be a nicer way to write this
eval :: Rule -> (Cell,Cell,Cell) -> Cell
eval r (h,m,l) = r `testBit` ((h `asBit` 2) .|. (m `asBit` 1) .|. (l `asBit` 0))

expandShrink :: Rule -> [Cell] -> [Cell]
expandShrink rule = go where
  go (h:cs@(m:l:_)) = eval rule (h,m,l) : go cs
  go _ = []

data Padding = Padding { addCells     :: !Int
                       , leftPadding  :: !Cell
                       , rightPadding :: !Cell }
             deriving (Eq, Ord, Show, Read)

pattern Padding' :: Int -> Cell -> Padding
pattern Padding' addCells padding <- Padding { addCells
                                             , leftPadding  = padding
                                             , rightPadding = (== padding) -> True } where
  Padding' addCells leftPadding@rightPadding = Padding{..}

pattern PadStasis :: Cell -> Cell -> Padding
pattern PadStasis leftPadding rightPadding = Padding{addCells = 0, ..}

pattern PadStasis' :: Cell -> Padding
pattern PadStasis' padding = Padding' 0 padding

padCells :: Padding -> [Cell] -> [Cell]
padCells Padding{..} cells =
  let delta = addCells + 1
  in if delta < 0
     then drop delta $ take (length cells - delta) cells
     else replicate delta leftPadding ++ cells ++ replicate delta rightPadding

expand :: Rule -> Padding -> [Cell] -> [Cell]
expand rule padding = expandShrink rule . padCells padding

expansions :: Rule -> Padding -> [Cell] -> [[Cell]]
expansions = iterate .: expand

data HSV a = HSV { hue, saturation, value :: !a }
           deriving (Eq, Ord, Show, Read)

setHSV :: (MonadState (HSV Double) m, OrderedField n) => HSV Double -> TurtleT n m ()
setHSV c@HSV{..} = do
  setPenColor $ hsv hue saturation value
  lift . put $ c

modifyHSV :: (MonadState (HSV Double) m, OrderedField n) => (HSV Double -> HSV Double) -> TurtleT n m ()
modifyHSV f = do
  c <- lift get
  setHSV $ f c

setSaturation :: (MonadState (HSV Double) m, OrderedField n) => Double -> TurtleT n m ()
setSaturation s = modifyHSV \c -> c{saturation = s}

stepHue :: (MonadState (HSV Double) m, OrderedField n) => Double -> TurtleT n m ()
stepHue θ = modifyHSV \c -> c{hue = hue c + θ}

data TurtleRules m = TurtleRules { on             :: m ()
                                 , off            :: m ()
                                 , nextGeneration :: Int -> m () }

draw1 :: Monad m => TurtleRules m -> [Cell] -> m ()
draw1 TurtleRules{..} = traverse_ \case { O -> off ; I -> on }

draw :: Monad m => TurtleRules m -> [[Cell]] -> m ()
draw tr = traverse_ (bitraverse_ sep $ draw1 tr) . zip [0..] where
  sep 0 = pure ()
  sep g = nextGeneration tr g

colorRules :: MonadState (HSV Double) m
           => Double -> Double -> Double -> Double -> TurtleRules (TurtleT Double m)
colorRules d s θ h = TurtleRules
  { on             =         setSaturation 1                           *> forward d
  , off            =         setSaturation 0.5               *> left θ *> forward d
  , nextGeneration = const $ setSaturation 0.25 *> stepHue h           *> forward (s*d) }

drawHSVTurtle :: forall b n.
                 (Renderable (Path V2 n) b, TypeableFloat n)
              => HSV Double -> TurtleT n (State (HSV Double)) () -> QDiagram b V2 n Any
drawHSVTurtle c₀ t = flip evalState c₀ . drawTurtleT $ setHSV c₀ *> t

-- The final idea: interpret successive generations of Rule 30 as turtle
-- instructions, à la drawing an L-system.  "On" is "forward" and "off" is
-- "rotate 30°, then forward".  At every generation, we stop, rotate around a
-- circle, and zip forward 10 times as far.  (The specific "around a circle"
-- part is flexible; we could instead rotate by 0° in order to move forward on a
-- line.  But we don't.)  The circle is tuned to come back to the starting angle
-- after we've taken all our steps.

data DisplayParameters
  = DisplayParameters { stepSize, stepAngle , generationScale, generationAngle :: !Double }
  deriving (Eq, Ord, Show, Read)

data Parameters = Parameters { rule              :: !Word8
                             , padding           :: !Padding
                             , start             :: ![Cell]
                             , generations       :: !Int
                             , displayParameters :: !DisplayParameters }
                deriving (Eq, Ord, Show, Read)

rules :: Monad m => DisplayParameters -> TurtleRules (TurtleT Double m)
rules DisplayParameters{..} = TurtleRules
  { on             =                   forward stepSize
  , off            = left stepAngle *> forward stepSize
  , nextGeneration = \g -> setHeading (generationAngle * (fromIntegral g-1))
                        *> forward (generationScale*stepSize) }

drawWith :: Monad m => Parameters -> TurtleT Double m ()
drawWith Parameters{..} = do
  penDown
  setPenWidth 0.5
  draw (rules displayParameters) . take generations $ expansions rule padding start

generationCircleAngle :: Int -> Double
generationCircleAngle generations = 360 / fromIntegral (1 `max` generations)

data WindingMode = StraightLine | Circle deriving (Eq, Ord, Enum, Bounded, Show, Read)

defaultParameters' :: WindingMode -> Int -> Double -> Parameters
defaultParameters' windingMode generations stepAngle = Parameters
  { rule              = 30
  , padding           = Padding' 1 O
  , start             = [I]
  , generations
  , displayParameters = DisplayParameters
      { stepSize        = 1
      , stepAngle   
      , generationScale = 20
      , generationAngle = case windingMode of
                            StraightLine -> 0
                            Circle       -> generationCircleAngle generations } }

defaultParameters :: Parameters
defaultParameters = defaultParameters' Circle 50 30.0

-- squared :: (Transformable a, Enveloped a, V a ~ V2) => a -> a
squared :: (OrderedField n, Monoid m) => QDiagram b V2 n m -> QDiagram b V2 n m
squared x = let _dim = max <$> width <*> height $ x
            in x # extrudeTop 7.8 # extrudeBottom 7.8
               -- x # sized (dims2D 5 10)

main :: IO ()
main = mainWith . bg white . pad 1.02 . resize . centerXY . drawTurtle @_ @B . drawWith $ defaultParameters
  where resize = extrudeTop 7.8 . extrudeBottom 7.8
        -- I don't know why I need this, but without it there are transparent
        -- bands above and below the bounding box.  The numbers were derived by
        -- trial and error.
