{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Genuary2021.January10 where

import Data.Word
import Data.List (isSuffixOf)

import Data.Typeable
import Data.Function
import Genuary2021.Util.Function
import Data.Biapplicative
import Control.Monad

import Data.Random
import Genuary2021.Util.Random.Source.PCG
import System.Random.PCG.Class (sysRandom)

import Data.List.NonEmpty (NonEmpty(..))
import Data.Urn.Internal.AlmostPerfect

import Data.Colour
import HSLuv
import Genuary2021.Util.Color

import Diagrams.Prelude
import qualified Diagrams.Backend.CmdLine as CLI
import Diagrams.Backend.SVG

import Options.Applicative
import System.Exit

-- Prompt:
-- // TREE

--------------------------------------------------------------------------------
-- Annotated trees

-- These trees are exactly like urns, except that 'ATree' is called
-- 'Data.Urn.Internal.WTree'.

data BTree a v = BLeaf v
               | BNode !(ATree a v) !(ATree a v)
               deriving (Eq, Ord, Show)

data ATree a v = ATree { annotation :: a
                       , btree      :: !(BTree a v) }
               deriving (Eq, Ord, Show)

pattern ALeaf :: a -> v -> ATree a v
pattern ANode :: a -> ATree a v -> ATree a v -> ATree a v
pattern ALeaf a v   = ATree { annotation = a, btree = BLeaf v }
pattern ANode a l r = ATree { annotation = a, btree = BNode l r }
{-# COMPLETE ALeaf, ANode #-}

fromNonEmpty :: (a -> a -> a) -> (i -> (a,v)) -> NonEmpty i -> ATree a v
fromNonEmpty merge leaf is =
  almostPerfect (\l r -> ANode ((merge `on` annotation) l r) l r)
                (uncurry ALeaf . leaf)
                (fromIntegral $ length is)
                is

fromNonEmpty1 :: (a -> a -> a) -> NonEmpty a -> ATree a a
fromNonEmpty1 = flip fromNonEmpty $ join (,)

fromNonEmptyPairs :: (a -> a -> a) -> NonEmpty (a,v) -> ATree a v
fromNonEmptyPairs = flip fromNonEmpty id

showATreeStructureWith :: (a -> String) -> (v -> String) -> ATree a v -> String
showATreeStructureWith dispAnnotation dispValue = unlines . strings
  where
    strings (ALeaf a v)   = ["(" ++ dispAnnotation a ++ ": " ++ dispValue v ++ ")"]
    strings (ANode a l r) = ("[" ++ dispAnnotation a ++ "]") :
                            " |" :
                            nest '+' '|' (strings l) ++
                            " |" :
                            nest '`' ' ' (strings r)

    nest cc gc (child:grandchildren) =
      ([' ',cc,'-'] ++ child) : map ([' ', gc, ' '] ++) grandchildren
    nest _ _ [] = []

showATreeStructure :: (Show a, Show v) => ATree a v -> String
showATreeStructure = showATreeStructureWith show show

--------------------------------------------------------------------------------

data Orientation = Horizontal | Vertical
               deriving (Eq, Ord, Enum, Bounded, Show, Read)

flipO :: Orientation -> Orientation
flipO Horizontal = Vertical
flipO Vertical   = Horizontal

-- H tree: <https://en.wikipedia.org/wiki/H_tree>
htree :: (Floating n, Semigroup p)
      => (P2 n -> P2 n -> p)
      -> (a -> v -> p -> p) -> (a -> p -> p)
      -> Orientation -> n -> P2 n
      -> ATree a v -> p
htree drawLine decorateLeaf decorateNode = go where
  go dir d pt = \case
    ALeaf a v   -> decorateLeaf a v line
    ANode a l r -> htree' pt₁ l <> htree' pt₂ r <> decorateNode a line
    where
      line = drawLine pt₁ pt₂
      pt₁ = pt ^-^ dpt
      pt₂ = pt ^+^ dpt
      htree' = go (flipO dir) (d / sqrt 2)
      dpt = case dir of
        Horizontal -> p2 (d,0)
        Vertical   -> p2 (0,d)

replicateTwoToThe :: Int -> a -> NonEmpty a
replicateTwoToThe n x = x :| replicate (2^n - 1) x

replicateATwoToThe :: Applicative f => Int -> f a -> f (NonEmpty a)
replicateATwoToThe = sequenceA .: replicateTwoToThe

drawHTree :: (TrailLike p, Semigroup p, V p ~ V2, HasStyle p)
          => (a -> p -> p) -> ATree a b -> p
drawHTree decorate = htree (~~) (const . decorate) decorate Horizontal 1 origin # lineCap LineCapButt

data TreeDrawing f a p = TreeDrawing { generate :: Int -> f a
                                     , combine  :: (a -> a -> a)
                                     , decorate :: a -> p -> p }

drawBoth :: Applicative f => TreeDrawing f a p -> TreeDrawing f b p -> TreeDrawing f (a, b) p
drawBoth td₁ td₂ = TreeDrawing { generate = \n -> (,) <$> generate td₁ n <*> generate td₂ n
                               , combine  = biliftA2 (combine td₁) (combine td₂)
                               , decorate = \(a₁,a₂) -> decorate td₁ a₁ . decorate td₂ a₂ }

renderTreeDrawing :: (TrailLike p, Semigroup p, HasStyle p, V p ~ V2, Applicative f)
                  => TreeDrawing f a p -> Int -> f p
renderTreeDrawing TreeDrawing{..} n =
  fmap (drawHTree decorate . fromNonEmpty1 combine) $ replicateATwoToThe n (generate n)

average :: Fractional a => a -> a -> a
average x y = (x+y)/2

thickening :: (Fractional (N p), Distribution Normal (N p), Typeable (N p), HasStyle p)
           => TreeDrawing (RVarT m) (N p) p
thickening = TreeDrawing { generate = \_ -> normalT 0.075 0.025
                         , combine  = (+)
                         , decorate = lw . output }

intenseLightness :: Double
intenseLightness = 0.65

colorWithSaturation :: (Floating (N p), V p ~ V2, Typeable (N p), HasStyle p)
                    => Double -> TreeDrawing (RVarT m) (Colour Double) p
colorWithSaturation s = TreeDrawing { generate = \_ -> hue <$> uniformT 0 360
                                    , combine  = blend 0.5
                                    , decorate = lc }
  where hue h = hsluv h s intenseLightness

color :: (Floating (N p), V p ~ V2, Typeable (N p), HasStyle p) => TreeDrawing (RVarT m) (Colour Double) p
color = colorWithSaturation 1

fullColor :: (Floating (N p), V p ~ V2, Typeable (N p), HasStyle p) => TreeDrawing (RVarT m) (Colour Double) p
fullColor = TreeDrawing { generate = \_ -> hsluv <$> uniformT 0 360 <*> uniformT 0 1 <*> uniformT 0.25 0.75
                        , combine  = blend 0.5
                        , decorate = lc }

stabilizing :: (Fractional (N p), Distribution Uniform (N p), Typeable (N p), HasStyle p)
            => TreeDrawing (RVarT m) (N p) p
stabilizing = TreeDrawing { generate = \_ -> uniformT 3 9
                          , combine  = average
                          , decorate = lw . output }

lightness :: (Floating (N p), Typeable (N p), HasStyle p, V p ~ V2)
           => Double -> TreeDrawing (RVarT m) Double p
lightness h = TreeDrawing { generate = \_ -> uniformT 0 1
                          , combine  = average
                          , decorate = \l -> lc $ hsluv h 1 l }

stabilizingColor :: (Floating (N p), Distribution Uniform (N p), V p ~ V2, Typeable (N p), HasStyle p)
                 => TreeDrawing (RVarT m) (N p, Colour Double) p
stabilizingColor = drawBoth stabilizing $ colorWithSaturation 0.25

stabilizingLightness :: (Floating (N p), Distribution Uniform (N p), V p ~ V2, Typeable (N p), HasStyle p)
                     => TreeDrawing (RVarT m) (N p, Double) p
stabilizingLightness = drawBoth stabilizing $ lightness 120
-- Goes well with @bgFrame 0.05 gray@

hueBlendWithSaturation :: (Floating (N p), V p ~ V2, Typeable (N p), HasStyle p)
                       => Double -> TreeDrawing (RVarT m) (Colour Double) p
hueBlendWithSaturation s = (colorWithSaturation s) {
                             combine = \c₁ c₂ ->
                               case colourToHsluv $ blend 0.5 c₁ c₂ of
                                 HSLuv (HSLuvHue h) _ _ -> hsluv h 1 intenseLightness
                           }

hueBlend :: (Floating (N p), V p ~ V2, Typeable (N p), HasStyle p) => TreeDrawing (RVarT m) (Colour Double) p
hueBlend = hueBlendWithSaturation 1

stabilizingHue :: (Floating (N p), Distribution Uniform (N p), V p ~ V2, Typeable (N p), HasStyle p)
               => TreeDrawing (RVarT m) (N p, Colour Double) p
stabilizingHue = drawBoth stabilizing hueBlend

intensifying :: (Floating (N p), V p ~ V2, Typeable (N p), HasStyle p)
             => TreeDrawing (RVarT m) HSLuv p
intensifying = TreeDrawing
  { generate = \n -> do
      h <- HSLuvHue <$> uniformT 0 360
      s <- HSLuvSaturation . (100*) <$> uniformT 0 (recip $ fromIntegral n)
      pure $ HSLuv h s lightness
  , combine = \c₁ c₂ ->
      let HSLuv _ (HSLuvSaturation s₁) _ = c₁
          HSLuv _ (HSLuvSaturation s₂) _ = c₂
          HSLuv h' _ _ = colourToHsluv $ (blend 0.5 `on` hsluvToColour) c₁ c₂
      in HSLuv h'
               (HSLuvSaturation . (100*) $ 1 - ((1 - s₁/100) * (1 - s₂/100)))
               lightness
  , decorate = lc . hsluvToColour }
  where lightness = HSLuvLightness $ 100*intenseLightness

stabilizingIntensifying :: (Floating (N p), Distribution Uniform (N p), V p ~ V2, Typeable (N p), HasStyle p)
                        => TreeDrawing (RVarT m) (N p, HSLuv) p
stabilizingIntensifying = drawBoth stabilizing intensifying

--------------------------------------------------------------------------------

-- I've now used this sort of argument parsing three times; it needs to be
-- abstracted over

data Seed = Genuary2021Seed
          | ExplicitSeed !Word64 !Word64
          | RandomSeed
          deriving (Eq, Ord, Show, Read)

genuary2021Seed :: (Word64, Word64)
genuary2021Seed = (18252809676486911832, 16014788316994255831)

getSeed :: Seed -> IO (Word64, Word64)
getSeed Genuary2021Seed            = pure genuary2021Seed
getSeed (ExplicitSeed seed1 seed2) = pure (seed1, seed2)
getSeed RandomSeed                 = (,) <$> sysRandom <*> sysRandom

seedParser :: Parser Seed
seedParser = pure Genuary2021Seed <|> subcommands
  where
    genuary2021 = info (pure Genuary2021Seed)
                       (progDesc "Generate the picture I used for Genuary 2021 (default)")
    seeded      = info (ExplicitSeed <$> argument auto (metavar "SEED1")
                                     <*> argument auto (metavar "SEED2"))
                       (progDesc "Generate a picture with a given seed")
    random      = info (pure RandomSeed)
                       (progDesc "Generate a picture with a random seed, and print out that seed")
     
    subcommands = hsubparser $ mconcat [ command "genuary2021" genuary2021
                                       , command "seeded"      seeded
                                       , command "random"      random ]

argumentsInfo :: ParserInfo (Seed, CLI.DiagramOpts)
argumentsInfo = info (((,) <$> seedParser <*> CLI.diagramOpts) <**> helper')
                     (fullDesc <> progDesc helpText)
  where
    helpText = "Draw an H tree with color that fades and thickness that grows as it extends out.  \
               \The color and thicknesses are built up into a perfect binary tree using the \
               \almost-perfect tree algorithm from /Ode on a Random Urn/, by Leonidas Lampropoulos, \
               \Antal Spector-Zabusky (hi!), and Kenneth Foner; see the `urn-random` package for an \
               \implementation."
    -- Copied out of the internals of "Diagrams.Backend.CmdLine"
    helper' = abortOption ShowHelpText $ mconcat
      [ long "help"
      , short '?'
      , help "Show this help text"
      ]

drawMainWith :: Seed -> IO (Diagram B)
drawMainWith seed = do
  (seed1,seed2) <- getSeed seed
  when (seed == RandomSeed) $
    putStrLn $ "Seed: " ++ show seed1 ++ " " ++ show seed2
  
  gen <- initialize seed1 seed2
  fmap (bgFrame 0.05 white) . sampleFrom @RVar gen $ renderTreeDrawing stabilizingIntensifying 8

main :: IO ()
main = do
  (seed, diagramOpts) <- execParser argumentsInfo
  unless (".svg" `isSuffixOf` (diagramOpts^.CLI.output)) do
    die "Unknown file type; must specify a \".svg\" file"
  renderSVG (diagramOpts^.CLI.output)
            (fromIntegral <$> mkSizeSpec2D (diagramOpts^.CLI.width) (diagramOpts^.CLI.height))
    =<< drawMainWith seed
