{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module Genuary2021.January03 where

-- Prompt: Make something human.
-- (Took 2 days)

-- The name "Outer Paean" is a pun on "Euterpe" in the same vein as "utopia".
-- Euterpe is the muse of music and lyric poetry; her name comes from, per
-- Wikipedia, "εὖ" + "τέρπειν", "good" + "to please" (or "delight").  Utopia can
-- be analyzed as "eu-topia", good place; or "(o)u-topia", "no place".  These
-- poems come from nowhere, so they are not "Euterpean" (coming from Euterpe)
-- but "Outerpean", which we can then reanalyze.

import Prelude hiding (Word)

import GHC.Generics

import Data.Functor
import Data.Foldable
import Data.Traversable
import Data.Bifunctor
import Data.Bitraversable
import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.State
import Genuary2021.Util.Monad

import Data.Semigroup
import Data.Maybe
import Data.Ord
import Data.Function
import Data.Char
import Data.Word (Word64)
import Data.List (partition, intercalate)
import Genuary2021.Util.Ord
import Genuary2021.Util.Function
import Genuary2021.Util.Tuple
import Genuary2021.Util.List

import Data.Set (Set())
import qualified Data.Set as S
import Data.Map.Strict (Map())
import qualified Data.Map.Strict as M
import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NEL

import qualified Data.Urn as Urn
import Data.Random
import Data.Random.Distribution.Bernoulli
import Data.Random.Distribution.Categorical hiding (fromList, toList)
import Data.Random.Distribution.Uniform
import System.Random.PCG.Class (sysRandom)
import Genuary2021.Util.Random
import Genuary2021.Util.Random.Source.PCG

import Data.Universe.Class
import Genuary2021.Util.Universe

import Options.Applicative

-- English-ish
data Place = Bilabial | Alveolar | Palatal | Velar | Glottal
           deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

-- English-ish
data Manner = Stop | Nasal | Fricative | Affricate | Approximant | Lateral
            deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

compareSonority :: Manner -> Manner -> Ordering
compareSonority = comparing @Int sonority where
  sonority Stop        = 0
  sonority Nasal       = 1
  sonority Affricate   = 2
  sonority Fricative   = 3
  sonority Lateral     = 4
  sonority Approximant = 5

data Voicing = Unvoiced | Voiced deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

data Consonant = Consonant { voicing :: !Voicing
                           , place   :: !Place
                           , manner  :: !Manner }
             deriving (Eq, Ord, Generic, Show, Read)
             deriving (Universe, Finite) via ViaGFinite Consonant

illegal :: Consonant -> Bool
illegal (Consonant Unvoiced _        Approximant) = True -- "wh" notwithstanding
illegal (Consonant _        Bilabial Lateral)     = True
illegal (Consonant Voiced   Glottal  Stop)        = True
illegal (Consonant _        Glottal  Nasal)       = True
illegal (Consonant Voiced   Glottal  Affricate)   = True
illegal (Consonant _        Glottal  Approximant) = True
illegal (Consonant _        Glottal  Lateral)     = True
illegal _                                         = False

data CardinalVowel = LowCentral
                   | MidFront  | MidBack
                   | HighFront | HighBack
                   deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

data Height   = Low  | Mid     | High  deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)
data Backness = Back | Central | Front deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

vowelFeatures :: CardinalVowel -> (Height, Backness)
vowelFeatures LowCentral = (Low,  Central)
vowelFeatures MidFront   = (Mid,  Front)
vowelFeatures MidBack    = (Mid,  Back)
vowelFeatures HighFront  = (High, Front)
vowelFeatures HighBack   = (High, Back)

data Roundedness  = Spread | Rounded   deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)
data Nasalization = Oral   | Nasalized deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

data Vowel = Vowel { cardinal     :: !CardinalVowel
                   , roundedness  :: !Roundedness
                   , nasalization :: !Nasalization }
           deriving (Eq, Ord, Generic, Show, Read)
           deriving (Universe, Finite) via ViaGFinite Vowel
               
data Phoneme = ConsonantPhoneme Consonant
             | VowelPhoneme     Vowel
             deriving (Eq, Ord, Generic, Show, Read)
             deriving (Universe, Finite) via ViaGFinite Phoneme

class HasPhonemes a where
  phonemes :: a -> NonEmpty Phoneme

instance HasPhonemes Consonant where
  phonemes = pure . ConsonantPhoneme

instance HasPhonemes Vowel where
  phonemes = pure . VowelPhoneme

instance HasPhonemes Phoneme where
  phonemes = pure

instance HasPhonemes a => HasPhonemes (NonEmpty a) where
  phonemes = sconcat . fmap phonemes

-- A good transcription scheme is /critical/ for legibility of the output.
-- Non-affricates should be one character; judicious use of diacritics is worth
-- it.  The transcription should also cause an English speaker to read the
-- output adequately; this drives several choices, like using ⟨ñ⟩ for /ɲ/, ⟨f⟩
-- and ⟨v⟩ for /ɸ/ and /ꞵ/, and so on.  We use "🚫" for illegal consonants so we
-- don't have to think about them.

class Transliterate a where
  transliterate :: a -> String

instance Transliterate Consonant where
  -- Bilabial
  transliterate (Consonant Unvoiced Bilabial Stop)        = "p"
  transliterate (Consonant Voiced   Bilabial Stop)        = "b"
  transliterate (Consonant Unvoiced Bilabial Nasal)       = "m̱" -- Instead of "m̥" -- maybe more
                                                                -- natural-looking?  The same for all the
                                                                -- nasals and laterals.
  transliterate (Consonant Voiced   Bilabial Nasal)       = "m"
  transliterate (Consonant Unvoiced Bilabial Fricative)   = "f"
  transliterate (Consonant Voiced   Bilabial Fricative)   = "v"
  transliterate (Consonant Unvoiced Bilabial Affricate)   = "pf"
  transliterate (Consonant Voiced   Bilabial Affricate)   = "bv"
  transliterate (Consonant Unvoiced Bilabial Approximant) = "🚫"
  transliterate (Consonant Voiced   Bilabial Approximant) = "w"
  transliterate (Consonant Unvoiced Bilabial Lateral)     = "🚫"
  transliterate (Consonant Voiced   Bilabial Lateral)     = "🚫"
  -- Alveolar
  transliterate (Consonant Unvoiced Alveolar Stop)        = "t"
  transliterate (Consonant Voiced   Alveolar Stop)        = "d"
  transliterate (Consonant Unvoiced Alveolar Nasal)       = "ṉ"
  transliterate (Consonant Voiced   Alveolar Nasal)       = "n"
  transliterate (Consonant Unvoiced Alveolar Fricative)   = "s"
  transliterate (Consonant Voiced   Alveolar Fricative)   = "z"
  transliterate (Consonant Unvoiced Alveolar Affricate)   = "ts"
  transliterate (Consonant Voiced   Alveolar Affricate)   = "dz"
  transliterate (Consonant Unvoiced Alveolar Approximant) = "🚫"
  transliterate (Consonant Voiced   Alveolar Approximant) = "r"
  transliterate (Consonant Unvoiced Alveolar Lateral)     = "ḻ"
  transliterate (Consonant Voiced   Alveolar Lateral)     = "l"
  -- Palatal
  transliterate (Consonant Unvoiced Palatal  Stop)        = "c"
  transliterate (Consonant Voiced   Palatal  Stop)        = "j" -- Oddly asymmetric with "c", but close enough
  transliterate (Consonant Unvoiced Palatal  Nasal)       = "ṉ̃"
  transliterate (Consonant Voiced   Palatal  Nasal)       = "ñ"
  transliterate (Consonant Unvoiced Palatal  Fricative)   = "ç" -- Like IPA; could also use "š" or similar
  transliterate (Consonant Voiced   Palatal  Fricative)   = "z̧" -- Like "ç"; no good standard
  transliterate (Consonant Unvoiced Palatal  Affricate)   = "cç"
  transliterate (Consonant Voiced   Palatal  Affricate)   = "jz̧"
  transliterate (Consonant Unvoiced Palatal  Approximant) = "🚫"
  transliterate (Consonant Voiced   Palatal  Approximant) = "y"
  transliterate (Consonant Unvoiced Palatal  Lateral)     = "ḻ̃"
  transliterate (Consonant Voiced   Palatal  Lateral)     = "l̃" -- Like "ñ"; could be misinterpreted as
                                                                -- nasalized, but there's no better choice
  -- Velar
  transliterate (Consonant Unvoiced Velar    Stop)        = "k"
  transliterate (Consonant Voiced   Velar    Stop)        = "g"
  transliterate (Consonant Unvoiced Velar    Nasal)       = "ŋ̄" -- Instead of "ŋ̱", to avoid colliding with the
                                                                -- descender
  transliterate (Consonant Voiced   Velar    Nasal)       = "ŋ"
  transliterate (Consonant Unvoiced Velar    Fricative)   = "x"
  transliterate (Consonant Voiced   Velar    Fricative)   = "ǧ" -- Not original to me
  transliterate (Consonant Unvoiced Velar    Affricate)   = "kx"
  transliterate (Consonant Voiced   Velar    Affricate)   = "gǧ"
  transliterate (Consonant Unvoiced Velar    Approximant) = "🚫"
  transliterate (Consonant Voiced   Velar    Approximant) = "w̌" -- Like "ǧ"
  transliterate (Consonant Unvoiced Velar    Lateral)     = "ɫ̄"
  transliterate (Consonant Voiced   Velar    Lateral)     = "ɫ" -- Velar insteaad of velarized
  -- Glottal
  transliterate (Consonant Unvoiced Glottal  Stop)        = "'"
  transliterate (Consonant Voiced   Glottal  Stop)        = "🚫"
  transliterate (Consonant Unvoiced Glottal  Nasal)       = "🚫"
  transliterate (Consonant Voiced   Glottal  Nasal)       = "🚫"
  transliterate (Consonant Unvoiced Glottal  Fricative)   = "h"
  transliterate (Consonant Voiced   Glottal  Fricative)   = "ḥ" -- Inspired by "ḥ" in some Hebrew
                                                                -- transliterations, but repurpposed
  transliterate (Consonant Unvoiced Glottal  Affricate)   = "'h"
  transliterate (Consonant Voiced   Glottal  Affricate)   = "🚫"
  transliterate (Consonant Unvoiced Glottal  Approximant) = "🚫"
  transliterate (Consonant Voiced   Glottal  Approximant) = "🚫"
  transliterate (Consonant Unvoiced Glottal  Lateral)     = "🚫"
  transliterate (Consonant Voiced   Glottal  Lateral)     = "🚫"

instance Transliterate Vowel where
  -- High front
  transliterate (Vowel HighFront  Spread  Oral)      = "i"
  transliterate (Vowel HighFront  Spread  Nasalized) = "ĩ"
  transliterate (Vowel HighFront  Rounded Oral)      = "ü"
  transliterate (Vowel HighFront  Rounded Nasalized) = "ü̃"
  -- High back
  transliterate (Vowel HighBack   Spread  Oral)      = "î" -- Less confusing than dotless "i" or "ī"
  transliterate (Vowel HighBack   Spread  Nasalized) = "î̃"
  transliterate (Vowel HighBack   Rounded Oral)      = "u"
  transliterate (Vowel HighBack   Rounded Nasalized) = "ũ"
  -- Mid front
  transliterate (Vowel MidFront   Spread  Oral)      = "e"
  transliterate (Vowel MidFront   Spread  Nasalized) = "ẽ"
  transliterate (Vowel MidFront   Rounded Oral)      = "ø"
  transliterate (Vowel MidFront   Rounded Nasalized) = "ø̃"
  -- Mid back
  transliterate (Vowel MidBack    Spread  Oral)      = "ô" -- As "î"
  transliterate (Vowel MidBack    Spread  Nasalized) = "ỗ"
  transliterate (Vowel MidBack    Rounded Oral)      = "o"
  transliterate (Vowel MidBack    Rounded Nasalized) = "õ"
  -- Low central
  transliterate (Vowel LowCentral Spread  Oral)      = "a"
  transliterate (Vowel LowCentral Spread  Nasalized) = "ã"
  transliterate (Vowel LowCentral Rounded Oral)      = "ä"
  transliterate (Vowel LowCentral Rounded Nasalized) = "ä̃"

instance Transliterate Phoneme where
  transliterate (ConsonantPhoneme c) = transliterate c
  transliterate (VowelPhoneme     v) = transliterate v

instance Transliterate (NonEmpty Phoneme) where
  transliterate = concatMap transliterate . NEL.toList

instance Transliterate [Phoneme] where
  transliterate = concatMap transliterate . foldMap (NEL.toList . phonemes)

isConsonant, isVowel :: Phoneme -> Bool
isConsonant (ConsonantPhoneme _) = True
isConsonant (VowelPhoneme     _) = False
isVowel     (ConsonantPhoneme _) = False
isVowel     (VowelPhoneme     _) = True

data VoicingDistinctions = NoVoicingDistinction Voicing
                         | NormalUnvoiced
                         | NormalDistinctions
                         | Distinctions Voicing (NonEmpty Manner) -- Some redundancy here
                         deriving (Eq, Ord, Generic, Show, Read)
                         deriving Universe via ViaGUniverse VoicingDistinctions

easilyUnvoiced :: Manner -> Bool
easilyUnvoiced Stop        = True
easilyUnvoiced Nasal       = False
easilyUnvoiced Fricative   = True
easilyUnvoiced Affricate   = True
easilyUnvoiced Approximant = False
easilyUnvoiced Lateral     = False

voicingsFor :: VoicingDistinctions -> Manner -> NonEmpty Voicing
voicingsFor (NoVoicingDistinction voicing) _ = pure voicing
voicingsFor NormalUnvoiced manner
  | easilyUnvoiced manner = pure Unvoiced
  | otherwise             = pure Voiced
voicingsFor NormalDistinctions manner
  | easilyUnvoiced manner = Unvoiced :| [Voiced]
  | otherwise             = pure Voiced
voicingsFor (Distinctions defaultVoicing distinctions) manner
  | manner `elem` distinctions = Unvoiced :| [Voiced]
  | otherwise                  = pure defaultVoicing

data RegularConsonantSystem = RegularConsonantSystem { voicings    :: !VoicingDistinctions
                                                     , places      :: !(NonEmpty Place)
                                                     , manners     :: !(NonEmpty Manner) }
                            deriving (Eq, Ord, Generic, Show, Read)
                            deriving Universe via ViaGUniverse RegularConsonantSystem

data VowelNumber = ThreeVowels | FiveVowels
                 deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

cardinalVowels :: VowelNumber -> NonEmpty CardinalVowel
cardinalVowels ThreeVowels = HighFront :| [HighBack, LowCentral]
cardinalVowels FiveVowels  = NEL.fromList [minBound..maxBound]

data VowelRoundednesses = NormalRoundednesses
                        | AllSpread
                        | AllRounded
                        | BothFrontRoundednesses
                        | BothBackRoundednesses
                        | AllRoundednesses
                        deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

normalRoundedness :: Height -> Backness -> Roundedness
normalRoundedness Low _       = Spread
normalRoundedness _   Front   = Spread
normalRoundedness _   Central = Spread
normalRoundedness _   Back    = Rounded

roundednessesForFeatures :: VowelRoundednesses -> Height -> Backness -> NonEmpty Roundedness
roundednessesForFeatures NormalRoundednesses    height backness = pure $ normalRoundedness height backness
roundednessesForFeatures AllSpread              _      _        = pure Spread
roundednessesForFeatures AllRounded             _      _        = pure Rounded
roundednessesForFeatures BothFrontRoundednesses _      Front    = Spread :| [Rounded]
roundednessesForFeatures BothFrontRoundednesses height backness = pure $ normalRoundedness height backness
roundednessesForFeatures BothBackRoundednesses  _      Back     = Spread :| [Rounded]
roundednessesForFeatures BothBackRoundednesses  height backness = pure $ normalRoundedness height backness
roundednessesForFeatures AllRoundednesses       _      _        = Spread :| [Rounded]

roundednessesFor :: VowelRoundednesses -> CardinalVowel -> NonEmpty Roundedness
roundednessesFor vowelRoundednesses = uncurry (roundednessesForFeatures vowelRoundednesses) . vowelFeatures

data RegularVowelSystem = RegularVowelSystem { vowelNumber        :: !VowelNumber
                                             , vowelRoundednesses :: !VowelRoundednesses
                                             , vowelNasalization  :: !Bool }
                        deriving (Eq, Ord, Generic, Show, Read)
                        deriving (Universe, Finite) via ViaGFinite RegularVowelSystem

data RegularInventory = RegularInventory { consonantSystem :: !RegularConsonantSystem
                                         , vowelSystem     :: !RegularVowelSystem }
                      deriving (Eq, Ord, Generic, Show, Read)
                      deriving Universe via ViaGUniverse RegularInventory

expandConsonantSystem :: RegularConsonantSystem -> [Consonant]
expandConsonantSystem RegularConsonantSystem{..} = NEL.filter (not . illegal) do
  place   <- places
  manner  <- manners
  voicing <- voicingsFor voicings manner
  pure Consonant{..}

expandVowelSystem :: RegularVowelSystem -> NonEmpty Vowel
expandVowelSystem RegularVowelSystem{..} = do
  cardinal     <- cardinalVowels vowelNumber
  roundedness  <- roundednessesFor vowelRoundednesses cardinal
  nasalization <- if vowelNasalization then Oral :| [Nasalized] else pure Oral
  pure Vowel{..}

expandRegularInventory :: RegularInventory -> ([Consonant], NonEmpty Vowel)
expandRegularInventory RegularInventory{..} = ( expandConsonantSystem consonantSystem
                                              , expandVowelSystem     vowelSystem )

oneOrMore :: Double -> NonEmpty (Urn.Weight, a) -> RVarT m (NonEmpty a)
oneOrMore p = go . Urn.fromNonEmpty where
  go urn = do
    (_, x, murn') <- Urn.remove urn
    continue      <- bernoulliT p
    (x :|) <$> case murn' of
                 Just urn' | continue -> NEL.toList <$> go urn'
                 _                    -> pure []

zeroOrMore :: Double -> NonEmpty (Urn.Weight, a) -> RVarT m [a]
zeroOrMore p weights = bernoulliT p >>= \case
                         True  -> NEL.toList <$> oneOrMore p weights
                         False -> pure []

randomVoicingDistinctions :: NonEmpty Manner -> RVarT m VoicingDistinctions
randomVoicingDistinctions manners =
  let randomDistinctions = Distinctions
        <$> boundedEnumStdUniform
        <*> oneOrMore (2/3) (fmap (annotating \m -> if easilyUnvoiced m then 3 else 1) manners)
  in maybe randomDistinctions pure =<< categoricalT @Double
       [ (3, Just NormalDistinctions)
       , (2, Just NormalUnvoiced)
       , (1, Just (NoVoicingDistinction Unvoiced))
       , (1, Just (NoVoicingDistinction Voiced))
       , (1, Nothing) ]

randomRegularConsonantSystem' :: RVarT m RegularConsonantSystem
randomRegularConsonantSystem' = do
  places   <- oneOrMore 0.9 $
                (4, Bilabial) :| [(5, Alveolar), (2, Palatal), (3, Velar), (1, Glottal)]
  manners  <- oneOrMore 0.9 $
                (10, Stop) :| [(7, Nasal), (5, Fricative), (3, Affricate), (3, Approximant), (1, Lateral)]
  voicings <- randomVoicingDistinctions manners
  pure RegularConsonantSystem{..}

randomRegularConsonantSystem :: RVarT m RegularConsonantSystem
randomRegularConsonantSystem = do
  consonants <- randomRegularConsonantSystem'
  if null $ expandConsonantSystem consonants
  then randomRegularConsonantSystem'
  else pure consonants

randomRegularVowelSystem :: RVarT m RegularVowelSystem
randomRegularVowelSystem =
  RegularVowelSystem <$> categoricalT @Double [ (3, FiveVowels)
                                              , (2, ThreeVowels) ]
                     <*> categoricalT @Double [ (5, NormalRoundednesses)
                                              , (4, AllSpread)
                                              , (4, AllRounded)
                                              , (3, BothFrontRoundednesses)
                                              , (2, BothBackRoundednesses)
                                              , (1, AllRoundednesses) ]
                     <*> bernoulliT @Double (1/8) -- Probability of nasalization distinction

randomRegularInventory :: RVarT m RegularInventory
randomRegularInventory = RegularInventory <$> randomRegularConsonantSystem <*> randomRegularVowelSystem

data Inventory = Inventory { regularInventory  :: !RegularInventory
                           , missingConsonants :: ![Consonant]
                           , missingVowels     :: ![Vowel] }
               deriving (Eq, Ord, Generic, Show, Read)
               deriving Universe via ViaGUniverse Inventory

randomInventory :: RVarT m (Inventory, NonEmpty Consonant, NonEmpty Vowel)
randomInventory = do
  regularInventory <- RegularInventory <$> randomRegularConsonantSystem <*> randomRegularVowelSystem
  let (consonants, vowels) = first NEL.fromList $ expandRegularInventory regularInventory
      missing p k phonemes = take (length phonemes `quot` k) <$> zeroOrMore p ((1,) <$> phonemes)
      atMostOneOver = id -- Documentation
  missingConsonants <- missing (2/3) (atMostOneOver 3) consonants
  missingVowels     <- missing (1/2) (atMostOneOver 4) vowels
  let here `exceptPhonemes` gone = NEL.fromList $ NEL.filter (`notElem` gone) here
  pure (Inventory{..}, consonants `exceptPhonemes` missingConsonants, vowels `exceptPhonemes` missingVowels)

data ConsonantClusterPattern = NoConsonants | OneConsonant | TwoSonorousConsonants | AnyTwoConsonants
                             deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

data NucleusPattern = OneVowel | Diphthong
                    deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

data ConsonantClusterWeights = ConsonantClusterWeights
  { noConsonants, oneConsonant, twoSonorousConsonants, anyTwoConsonants :: !Double }
  deriving (Eq, Ord, Generic, Show, Read)

data NucleusWeights = NucleusWeights { oneVowel, diphthong :: !Double }
                    deriving (Eq, Ord, Generic, Show, Read)

data ConsonantClusterPosition = Onset | Coda
                              deriving (Eq, Ord, Enum, Bounded, Generic, Universe, Finite, Show, Read)

randomConsonantClusterPattern :: ConsonantClusterWeights -> RVarT m ConsonantClusterPattern
randomConsonantClusterPattern ConsonantClusterWeights{..} = categoricalT
  [ (noConsonants,          NoConsonants)
  , (oneConsonant,          OneConsonant)
  , (twoSonorousConsonants, TwoSonorousConsonants)
  , (anyTwoConsonants,      AnyTwoConsonants)]

randomNucleusPattern :: NucleusWeights -> RVarT m NucleusPattern
randomNucleusPattern NucleusWeights{..} = categoricalT [(oneVowel, OneVowel), (diphthong, Diphthong)]

randomConsonantCluster :: (Ord p, Floating p, Distribution Uniform p)
                       => ConsonantClusterPattern -> ConsonantClusterPosition -> Zipfian p Consonant
                       -> RVarT m [Consonant]
randomConsonantCluster NoConsonants          _   _          = pure []
randomConsonantCluster OneConsonant          _   consonants = pure <$> zipfT consonants
randomConsonantCluster AnyTwoConsonants      _   consonants = do
  c₁ <- zipfT consonants
  (c₁ :) <$> case filterZipfian (/= c₁) consonants of
               Just consonants' -> pure <$> zipfT consonants'
               Nothing          -> pure []
randomConsonantCluster TwoSonorousConsonants pos consonants =
  tryNTimesOr 100 (pure <$> zipfT consonants) $ runMaybeT do
    let restrict  = MaybeT . pure . NEL.nonEmpty .: NEL.filter
        direction = case pos of
                      Onset -> LT
                      Coda  -> GT
    nonAffricates      <- restrict ((/= Affricate) . manner) $ orderedValues consonants
    c₁                 <- lift $ zipfT consonants{orderedValues = nonAffricates}
    sonorousConsonants <- restrict ((== direction) . (compareSonority `on` manner) c₁) nonAffricates
    c₂                 <- lift $ zipfT consonants{orderedValues = sonorousConsonants}
    pure [c₁,c₂]

randomNucleus :: (Ord p, Floating p, Distribution Uniform p)
              => NucleusPattern -> Zipfian p Consonant -> Zipfian p Vowel -> RVarT m (NonEmpty Phoneme)
randomNucleus OneVowel  _          vowels = pure . VowelPhoneme <$> zipfT vowels
randomNucleus Diphthong consonants vowels =
  case filterZipfian ((== Approximant) . manner) consonants of
    Nothing           -> pure . VowelPhoneme <$> zipfT vowels
    Just approximants -> sequence $  (VowelPhoneme     <$> zipfT vowels)
                                  :| [ConsonantPhoneme <$> zipfT approximants]

data Phonotactics = Phonotactics { onsetWeights   :: !ConsonantClusterWeights
                                 , nucleusWeights :: !NucleusWeights
                                 , codaWeights    :: !ConsonantClusterWeights }
                  deriving (Eq, Ord, Generic, Show, Read)

randomPhonotactics :: NonEmpty Consonant -> RVarT m Phonotactics
randomPhonotactics consonants = do
  let nonnegNormal m σ = atLeast 0 <$> normalT m σ
      
      chance' z p = fmap (fromMaybe z) . possiblyT @Double p
      chance = chance' 0
      
      ifAnyManner pred n | any (pred . manner) consonants = n
                         | otherwise                      = 0
  
  onsetWeights <- do
    noConsonants          <- chance 0.8                              $ nonnegNormal 40 8
    oneConsonant          <-                                           nonnegNormal 60 10 <&> atLeast 1
    twoSonorousConsonants <- chance (ifAnyManner (/= Affricate) 0.5) $ nonnegNormal 30 7
    anyTwoConsonants      <- chance 0.15                             $ nonnegNormal 10 3
    pure ConsonantClusterWeights{..}
  nucleusWeights <- do
    let oneVowel = 100
    diphthong <- chance (ifAnyManner (== Approximant) $ 1/3) $ nonnegNormal 50 10 <&> atLeast 1
    pure NucleusWeights{..}
  codaWeights <- chance' (ConsonantClusterWeights 0 0 0 0) (2/3) do
    noConsonants          <- chance 0.8                              $ nonnegNormal 60 10
    oneConsonant          <-                                           nonnegNormal 40 8 <&> atLeast 1
    twoSonorousConsonants <- chance (ifAnyManner (/= Affricate) 0.5) $ nonnegNormal 25 7
    anyTwoConsonants      <- chance 0.15                             $ nonnegNormal 7  2
    pure ConsonantClusterWeights{..}
  pure Phonotactics{..}

newtype Syllable = Syllable (NonEmpty Phoneme)
                 deriving (Eq, Ord, Generic, Show, Read)
                 deriving newtype (HasPhonemes, Transliterate, Universe)

randomSyllable :: (Ord p, Floating p, Distribution Uniform p)
               => Phonotactics -> Zipfian p Consonant -> Zipfian p Vowel -> RVarT m Syllable
randomSyllable Phonotactics{..} consonants vowels = do
  onsetPattern   <- randomConsonantClusterPattern onsetWeights
  nucleusPattern <- randomNucleusPattern          nucleusWeights
  codaPattern    <- randomConsonantClusterPattern codaWeights
  
  onset   <- randomConsonantCluster onsetPattern   Onset consonants
  nucleus <- randomNucleus          nucleusPattern       consonants vowels
  coda    <- randomConsonantCluster codaPattern    Coda  consonants

  pure . Syllable $ (ConsonantPhoneme <$> onset) ++| nucleus |++ (ConsonantPhoneme <$> coda)

-- Could be a type synonym for @'Normal' Double@, but I like the documentation
-- of the field names.
data WordSyllables = WordSyllables { meanSyllables, syllablesStdDev :: !Double }
                   deriving (Eq, Ord, Generic, Show, Read)

randomWordSyllables :: NonEmpty Consonant -> NonEmpty Vowel -> RVarT m WordSyllables
randomWordSyllables consonants vowels = do
  let maxPairs, pairs :: Double
      maxPairs = fromIntegral $ length (filter (not . illegal) universeF) * length (universeF @Vowel)
      pairs    = fromIntegral $ NEL.length consonants * NEL.length vowels
  -- The mean mean syllable count ranges from 1..3 as the number of
  -- consonants-vowel pairs decreases.  We leave our normal distribution fairly
  -- wide, so we don't need to go higher.  The mean standard deviation is far
  -- more ad hoc.
  meanSyllables      <- atLeast 1    <$> normalT (1 + 2*(1 - pairs/maxPairs)^(3::Int)) 0.5
  syllablesStdDev    <- atLeast 0.01 <$> normalT (sqrt meanSyllables / meanSyllables)  0.1
  pure WordSyllables{..}

randomSyllableCount :: WordSyllables -> RVarT m Int
randomSyllableCount WordSyllables{..} =
  atLeast 1 . round <$> normalT meanSyllables syllablesStdDev

newtype Word = Word (NonEmpty Syllable)
             deriving (Eq, Ord, Generic, Show, Read)
             deriving newtype (HasPhonemes, Universe)

instance Transliterate Word where
  transliterate = transliterate . phonemes

instance Transliterate [Word] where
  transliterate = unwords . map transliterate

instance Transliterate (NonEmpty Word) where
  transliterate = transliterate . NEL.toList

newtype Paragraph f a = Paragraph (f a)
                      deriving (Eq, Ord, Functor, Foldable, Traversable, Generic, Show, Read)
                      deriving newtype (Applicative, Monad, HasPhonemes, Universe)

instance (Foldable f, Transliterate a) => Transliterate (Paragraph f a) where
  transliterate = foldMap $ (++ "\n") . transliterate

newtype Capitalized a = Capitalized a
                      deriving (Eq, Ord, Generic, Show, Read)
                      deriving newtype (HasPhonemes, Universe)

instance Transliterate a => Transliterate (Capitalized a) where
  transliterate (Capitalized p) = case span (not . isAlpha) $ transliterate p of
                                    (pre, l : post) -> pre ++ toUpper l : post
                                    (pre, [])       -> pre

countSyllables :: Word -> Int
countSyllables (Word syllables) = NEL.length syllables

randomWord :: (Ord p, Floating p, Distribution Uniform p)
           => WordSyllables -> Phonotactics
           -> Zipfian p Consonant -> Zipfian p Vowel
           -> RVarT m Word
randomWord wordSyllables phonotactics consonants vowels = do
  n <- randomSyllableCount wordSyllables
  fmap (Word . NEL.fromList) . replicateM n $ randomSyllable phonotactics consonants vowels

data Language = Language { name          :: !(Capitalized Word)
                         , inventory     :: !Inventory
                         , consonants    :: !(Zipfian Double Consonant)
                         , vowels        :: !(Zipfian Double Vowel)
                         , phonotactics  :: !Phonotactics
                         , wordSyllables :: !WordSyllables
                         , lexicon       :: !(Zipfian Double Word) }
              deriving (Eq, Ord, Generic, Show, Read)

randomLanguage :: RVarT m Language
randomLanguage = do
  let randomNormalZipfian m σ xs = do
        a <- normalT m σ <&> atLeast 1
        randomZipfianT a xs
      
      splitFirst nel@(x :| [])   = (x, nel)
      splitFirst (x₁ :| x₂ : xs) = (x₁, x₂ :| xs)
  (inventory, unorderedConsonants, unorderedVowels) <- randomInventory
  consonants      <- randomNormalZipfian 1.007 0.0015 unorderedConsonants
  vowels          <- randomNormalZipfian 1.005 0.001  unorderedVowels
  phonotactics    <- randomPhonotactics unorderedConsonants
  wordSyllables   <- randomWordSyllables unorderedConsonants unorderedVowels
  wordCount       <- atLeast 100 . round <$> normalT @Double 1000 100
  (name, lexicon) <- bitraverse (pure . Capitalized) (randomNormalZipfian 1.01  0.002)
                       .   splitFirst . NEL.fromList
                       <=< replicateDistinct ReplicationStrategy{trials=100, onFailure=StopShort} (1+wordCount)
                       $   randomWord wordSyllables phonotactics consonants vowels
  pure Language{..}

trailingSyllable :: Word -> Syllable
trailingSyllable (Word syllables) = NEL.last syllables

randomWordSuchThat :: (Ord p, Floating p, Distribution Uniform p)
                   => Zipfian p Word -> (Word -> Bool) -> RVarT m (Maybe Word)
randomWordSuchThat lexicon pred = traverse zipfT $ filterZipfian pred lexicon

randomWordSuchThatOrElse :: (Ord p, Floating p, Distribution Uniform p)
                         => Zipfian p Word -> (Word -> Bool) -> RVarT m Word -> RVarT m Word
randomWordSuchThatOrElse lexicon pred orElse = maybe orElse pure =<< randomWordSuchThat lexicon pred

randomNonceRhyme :: Language -> Syllable -> RVarT m Word
randomNonceRhyme Language{..} rhyme = do
  Word prefix <- randomWord wordSyllables{meanSyllables = meanSyllables wordSyllables - 1}
                            phonotactics
                            consonants
                            vowels
  pure $ Word $ prefix <> pure rhyme

randomRhyme :: Language -> Word -> RVarT m Word
randomRhyme language target =
  let targetSyllable  = trailingSyllable target
      validRhyme word = word /= target && trailingSyllable word == targetSyllable
  in randomWordSuchThatOrElse (lexicon language) validRhyme $ randomNonceRhyme language targetSyllable

randomWordsWithSyllables :: Language -> Int -> RVarT m [Word]
randomWordsWithSyllables Language{..} = go where
  go remaining
    | remaining <= 0 = pure []
    | otherwise      = do
        (c, word) <- case filterZipfian ((<= remaining) . countSyllables) lexicon of
                       Just shorter -> do
                         word <- zipfT shorter
                         pure (countSyllables word, word)
                       Nothing      -> do
                         c    <- uniformT 1 remaining
                         word <- replicateM c (randomSyllable phonotactics consonants vowels)
                         pure (c, Word $ NEL.fromList word)
        (word :) <$> go (remaining - c)

randomWordsWithSyllablesBetween :: Language -> Int -> [Word] -> [Word] -> RVarT m [Word]
randomWordsWithSyllablesBetween language syllables prefix suffix = do
  words <- randomWordsWithSyllables language $ syllables - (sum . map countSyllables $ prefix ++ suffix)
  pure $ prefix ++ words ++ suffix

-- Slightly different than 'randomPoem': generates a whole first line, then
-- generates rhymes with that.  Compare with 'randomPoem', which generates a
-- word for each rhyme scheme, then always generates constrained lines.
randomCouplet :: Language -> Int -> RVarT m (NonEmpty Word, NonEmpty Word)
randomCouplet language n = do
  let atLeastOne = maybe (pure <$> zipfT (lexicon language)) pure . NEL.nonEmpty
  firstLine  <- atLeastOne =<< randomWordsWithSyllables language n
  rhyme      <- randomRhyme language $ NEL.last firstLine
  secondLine <- NEL.fromList <$> randomWordsWithSyllablesBetween language n [] [rhyme]
  pure (firstLine, secondLine)

transliterateCouplet :: (NonEmpty Word, NonEmpty Word) -> String
transliterateCouplet (firstLine, secondLine) =
  transliterate firstLine ++ "\n" ++ transliterate secondLine

randomCoupletPoem :: Language -> Int -> Int -> RVarT m (Paragraph NonEmpty (Capitalized (NonEmpty Word)))
randomCoupletPoem language couplets syllables =
  Paragraph
     .  NEL.fromList . map Capitalized
     .  concatMap (\(x,y) -> [x,y])
    <$> replicateM (atLeast 1 couplets) (randomCouplet language syllables)

newtype StanzaScheme = StanzaScheme (NonEmpty (Int, String))
                     deriving (Eq, Ord, Generic, Show, Read)
                     deriving newtype Universe

type Stanza = Paragraph NonEmpty (Capitalized (NonEmpty Word))

randomStanzaInContext :: Language -> StanzaScheme -> StateT (Map String (Syllable, Set Word)) (RVarT m) Stanza
randomStanzaInContext language (StanzaScheme lines) = Paragraph <$> traverse line lines
  where
    line (atLeast 1 -> syllables, rhymeKey) = Capitalized . NEL.fromList <$> do
      oldRhymes <- maybe (startRhyme rhymeKey) pure =<< gets (M.lookup rhymeKey)
      newRhyme  <- newRandomRhyme oldRhymes
      addRhyme rhymeKey newRhyme
      lift $ randomWordsWithSyllablesBetween language syllables [] [newRhyme]

    newRandomRhyme (rhymeSyllable, rhymeWords) = lift $
      randomWordSuchThatOrElse
        (lexicon language)
        (\word -> trailingSyllable word == rhymeSyllable && word `S.notMember` rhymeWords)
        (randomNonceRhyme language rhymeSyllable)
    
    startRhyme rhymeKey = do
      rhyme <- lift . zipfT $ lexicon language
      let rhymeInfo = (trailingSyllable rhyme, S.singleton rhyme)
      modify $ M.insert rhymeKey rhymeInfo
      pure rhymeInfo
    
    addRhyme rhymeKey newRhyme =
      modify $ M.adjust (second $ S.insert newRhyme) rhymeKey

randomStanza :: Language -> StanzaScheme -> RVarT m Stanza
randomStanza = flip evalStateT M.empty .: randomStanzaInContext

newtype PoemScheme = PoemScheme (NonEmpty StanzaScheme)
                   deriving (Eq, Ord, Generic, Show, Read)
                   deriving newtype Universe

newtype Poem = Poem (NonEmpty Stanza)
             deriving (Eq, Ord, Generic, Show, Read)
             deriving newtype (HasPhonemes, Universe)

instance Transliterate Poem where
  transliterate (Poem stanzas) = sconcat . NEL.intersperse "\n" $ transliterate <$> stanzas

randomPoem :: Language -> PoemScheme -> RVarT m Poem
randomPoem language (PoemScheme stanzas) =
  Poem <$> evalStateT (traverse (randomStanzaInContext language) stanzas) M.empty

-- Right now, we just print out the phoneme inventory and the lexicon nicely,
-- leaving the rest obscure; we print out the seed above, but the values are too
-- big to print into the file with 'show'.
latexLanguage :: Language -> String
latexLanguage language =
  let section name generator field =
        "\\section{" ++ name ++ "}\n" ++
        "" ++
        generator (orderedValues $ field language)
  in section "Consonants" consonantChart consonants ++
     "\n" ++
     section "Vowels" vowelChart vowels ++
     "\n" ++
     section "Lexicon" lexiconList lexicon
  
latexChapter :: Foldable f => String -> (a -> Language) -> (a -> String) -> f a -> String
latexChapter chapter language text infos = unlines
  $  [ "\\chapter{" ++ chapter ++ "}"
     , "" ]
  ++ concat [ [ "\\section{" ++ transliterate (name $ language info) ++ "}"
              , ""
              , text info ]
            | info <- toList infos ]

latexLanguages :: Foldable f => String -> f a -> (a -> Language) -> [(String, a -> String)] -> String
latexLanguages preamble infos language chapters = unlines
  $  [ "% !TeX program = lualatex"
     , "\\documentclass[12pt]{amsbook}"
     , ""
     , "\\usepackage{fontspec}"
     , "\\usepackage{fullpage}"
     , "\\usepackage{multicol}"
     , "\\usepackage{vowel}"
     , ""
     , "\\setmainfont{Noto Serif}"
     , "\\setlength{\\parindent}{0pt}"
     , "\\setlength{\\parskip}{\\baselineskip}"
     , "\\renewcommand*{\\arraystretch}{1.5}"
     , ""
     , preamble
     , ""
     , "\\title{Outer Paean}"
     , "\\date{}"
     , ""
     , "\\begin{document}"
     , ""
     , "\\maketitle"
     , ""
     , "\\begin{abstract}"
     , "Generated poems in " ++ theGeneratedLanguages ++ "."
     , "The poems are presented along with descriptions of these languages."
     , "\\end{abstract}"
     , "" ]
  ++ [ latexChapter chapter language text infos | (chapter, text) <- chapters ]
  ++ [ latexChapter "Phonemic inventories"
                    id
                    (\Language{..} -> "\\subsection{Consonants}\n"
                                   ++ "\n"
                                   ++ consonantChart (orderedValues consonants)
                                   ++ "\n"
                                   ++ "\\subsection{Vowels}\n"
                                   ++ vowelChart (orderedValues vowels))
                    languages
     , ""
     , latexChapter "Lexicons"
                    id
                    (lexiconList . orderedValues . lexicon)
                    languages
     , "\\end{document}" ]
  where
    languages = language <$> toList infos
    
    theGeneratedLanguages = english "no generated languages at all"
                                    "the generated language"
                                    "the generated languages"
                                    ","
                                    "and"
                          $ map (transliterate . name) languages

consonantChart :: Foldable f => f Consonant -> String
consonantChart (S.fromList . toList -> consonants) =
    "\\begin{center}\n" ++
    "\\begin{tabular}{" ++ tablespec ++ "}\n" ++
    (mannerCells . map (placeCells . map voicingCells) $ wholeTable) ++
    "\\end{tabular}\n" ++
    "\\end{center}\n"
  where
    entries :: Ord a => (Consonant -> a) -> [a]
    entries = S.toList . flip S.map consonants
      
    voicings = entries voicing
    places   = entries place
    manners  = entries manner

    showVoicings = case voicings of
                     [_,_] -> True
                     _     -> False
    
    tablespec = "r|" ++ concat (replicate (length places) if showVoicings then "cc|" else "c|")
    
    voicingCells | showVoicings = intercalate " & "
                | otherwise    = concat
    placeCells  = intercalate " & "
    mannerCells = concatMap $ ("  " ++) . (++ " \\\\\\hline\n")
    
    wholeTable =
      [ [ [ resolve voicing place manner
          | voicing <- if isNothing place || isNothing manner
                       then [Unvoiced]
                       else voicings ]
        | place <- Nothing : (Just <$> places) ]
      | manner <- Nothing : (Just <$> manners) ]
    
    label str = "\\emph{" ++ str ++ "}"
    
    resolve _       Nothing      Nothing       =
      ""
    resolve _       (Just place) Nothing
      | showVoicings = "\\multicolumn{2}{c|}{" ++ label (show place) ++ "}"
      | otherwise    = label $ show place
    resolve _       Nothing      (Just manner) =
      label $ show manner
    resolve voicing (Just place) (Just manner)
      | consonant `S.member` consonants = transliterate consonant
      | otherwise                       = ""
      where consonant = Consonant{..}

vowelChart :: Foldable f => f Vowel -> String
vowelChart vowels = ("\\begin{center}\n" ++) . (++ "\\end{center}\n") $
  case catMaybes [vowelDiagram orals, vowelDiagram nasals] of
    [oralDiagram, nasalDiagram] ->
         "\\begin{tabular}{cc}\n"
      ++ "\\emph{Oral vowels} & \\emph{Nasal vowels} \\\\\n"
      ++ oralDiagram ++ "&\n" ++ nasalDiagram
      ++ "\\end{tabular}\n" :: String
    diagrams ->
      concat diagrams
  where
    (orals, nasals) = join bimap vowelMap . partition ((== Oral) . nasalization) $ toList vowels
    vowelMap = foldr upd M.empty where
      upd vowel info = M.alter (Just . (vowel :) . maybe [] id) (cardinal vowel) info
    
    place HighFront  = ("\\putcvowel", "{1}")
    place HighBack   = ("\\putcvowel", "{8}")
    place MidFront   = ("\\putcvowel", "{2}")
    place MidBack    = ("\\putcvowel", "{7}")
    place LowCentral = ("\\putvowel",  "{3\\vowelhunit}{2\\vowelvunit}") 

    lr [_,_] Vowel{roundedness=Spread}  = "[l]"
    lr [_,_] Vowel{roundedness=Rounded} = "[r]"
    lr _     _                          = ""

    vowelDiagramInnards vmap = flip foldMap (M.toList vmap) \(cardinal, vowels) ->
        let (cmd, loc) = place cardinal
        in unlines [ "  " ++ cmd ++ lr vowels vowel  ++ "{" ++ transliterate vowel ++ "}" ++ loc
                   | vowel <- vowels ]
    
    vowelDiagram vmap
      | M.null vmap = Nothing
      | otherwise   = Just $  "\\begin{vowel}[simple,three]\n"
                           ++ vowelDiagramInnards vmap
                           ++ "\\end{vowel}\n"

lexiconList :: Foldable f => f Word -> String
lexiconList lexicon = unlines $
  [  "\\begin{multicols}{4}" ]
  ++ map ((++ " \\\\") . transliterate) (toList lexicon) ++
  [  "\\end{multicols}" ]

scheme_8_ABAB_6_CC :: PoemScheme
scheme_8_ABAB_6_CC = PoemScheme $  StanzaScheme ((8, "A") :| (8, "B") : (8, "A") : (8, "B") : [])
                                :| StanzaScheme ((6, "C") :| (6, "C") : [])
                                :  []

scheme_limerick :: PoemScheme
scheme_limerick = PoemScheme $  StanzaScheme ((8, "A") :| (8, "A") : (5, "B") : (5, "B") : (8, "A") : [])
                             :| []

scheme_12_ABAB_CACA_BABA_ACAC :: PoemScheme
scheme_12_ABAB_CACA_BABA_ACAC =
  let stanza r1 r2 = StanzaScheme $ (12, r1) :| (12, r2) : (12, r1) : (12, r2) : []
  in PoemScheme $ stanza "A" "B" :| stanza "C" "A" : stanza "B" "A" : stanza "A" "C" : []

scheme_10_couplet :: PoemScheme
scheme_10_couplet = PoemScheme $ StanzaScheme ((10, "A") :| (10, "A") : []) :| []

createPoems :: NonEmpty (NonEmpty (Int, PoemScheme)) -> RVarT m (NonEmpty (Language, NonEmpty Poem))
createPoems = traverse \schemes -> do
  language <- randomLanguage
  poems    <- sconcat <$> for schemes \(atLeast 1 -> k, scheme) ->
                fmap NEL.fromList . replicateM k $ randomPoem language scheme
  pure (language, poems)

genuary2021Seed :: (Word64, Word64)
genuary2021Seed = (11225054059764604297, 18362849590856618388)

data Arguments = Genuary2021Arg
               | SeededArg !Word64 !Word64
               | RandomArg
               deriving (Eq, Ord, Generic, Show, Read)
               deriving (Universe, Finite) via ViaGFinite Arguments

argumentsParser :: Parser Arguments
argumentsParser = pure Genuary2021Arg <|> subcommands
  where
    genuary2021 = info (pure Genuary2021Arg)
                       (progDesc "Generate the poems and languages for Genuary 2021 (default)")
    seeded      = info (SeededArg <$> argument auto (metavar "SEED1")
                                  <*> argument auto (metavar "SEED2"))
                       (progDesc "Generate some poems and languages with the given seed")
    random      = info (pure RandomArg)
                       (progDesc "Generate poems and languages with a random seed")
     
    subcommands = hsubparser $ mconcat [ command "genuary2021" genuary2021
                                       , command "seeded"      seeded
                                       , command "random"      random ]

argumentsInfo :: ParserInfo Arguments
argumentsInfo = info (argumentsParser <**> helper)
                     (fullDesc <> progDesc "Generate random poems in random languages")

mainWithSeed :: Word64 -> Word64 -> IO ()
mainWithSeed seed1 seed2 = do
  let seedComment = unwords ["% SEED:", show seed1, show seed2]
  gen    <- initialize seed1 seed2
  poetry <- sampleFrom @RVar gen . createPoems $  ((2, scheme_8_ABAB_6_CC) :| (2, scheme_limerick) : [])
                                               :| ((5, scheme_10_couplet) :| [])
                                               :  ((1, scheme_12_ABAB_CACA_BABA_ACAC) :| [])
                                               :  []
  putStr $ latexLanguages seedComment poetry fst
    [ ( "Poems", \(_language, poems) ->
          let texify ['\n']        = "\n"
              texify ('\n':'\n':s) = "\n\n"   ++ texify s
              texify ('\n':s)      = "\\\\\n" ++ texify s
              texify (c:s)         = c        :  texify s
              texify []            = []
          in concat (NEL.intersperse "\n*~*~*\n\n" $ texify . transliterate <$> poems) ) ]

mainWith :: Arguments -> IO ()
mainWith Genuary2021Arg          = uncurry mainWithSeed genuary2021Seed
mainWith (SeededArg seed1 seed2) = mainWithSeed seed1 seed2
mainWith RandomArg               = join $ mainWithSeed <$> sysRandom <*> sysRandom

main :: IO ()
main = mainWith =<< execParser argumentsInfo
