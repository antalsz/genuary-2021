{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module Genuary2021.January07 where

import Data.Foldable
import Data.Traversable
import Control.Monad
import Genuary2021.Util.Applicative

import Data.Word

import Text.Printf

import Data.Random
import Data.Random.Distribution.Uniform
import Genuary2021.Util.Random
import System.Random.PCG.Class (sysRandom)
import Genuary2021.Util.Random.Source.PCG

import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.Set as S

import Options.Applicative
import Options.Applicative.Help.Chunk
import Options.Applicative.Help.Pretty

-- Prompt: Generate some rules, then follow them by hand on paper.

data Count = One | Two | Three | Four | Five | Several
           deriving (Eq, Ord, Enum, Bounded, Show, Read)

data Shape = Circle | Triangle | Square | Blob | Polygon
           deriving (Eq, Ord, Enum, Bounded, Show, Read)

isRounded :: Shape -> Bool
isRounded Circle = True
isRounded Blob   = True
isRounded _      = False

data Location = InTheMiddle | NearTheEdges | InTheCorners | Somewhere
              deriving (Eq, Ord, Enum, Bounded, Show, Read)

data Relate = Connect | Separate
            deriving (Eq, Ord, Enum, Bounded, Show, Read)

data Relator = StraightLines | Arcs | LoopDeLoops | Diamonds
             deriving (Eq, Ord, Enum, Bounded, Show, Read)

data Targets = Edges | Corners | Shapes
             deriving (Eq, Ord, Enum, Bounded, Show, Read)

minForUnrounded, minForRounded :: Targets
minForUnrounded = Edges
minForRounded   = Corners

maxForSingular, maxForPlural :: Targets
maxForSingular = Corners
maxForPlural   = Shapes

data Subset = AllShapes
            | SomeShapes Shape
            deriving (Eq, Ord, Show, Read)

data Generate = Generate { count    :: !Count
                         , shape    :: !Shape
                         , location :: !Location }
              deriving (Eq, Ord, Show, Read)


data Action = Action { relate  :: !Relate
                     , subset  :: !Subset
                     , targets :: !Targets
                     , relator :: !Relator  }
            deriving (Eq, Ord, Show, Read)

data Rule = GenerateRule Generate | ActionRule Action
          deriving (Eq, Ord, Show, Read)

newtype Rules = Rules (NonEmpty Rule)
              deriving (Eq, Ord, Show, Read)

subsetShapes :: Applicative f => f Generate -> Subset -> f Shape
subsetShapes generators AllShapes      = shape <$> generators
subsetShapes _          (SomeShapes s) = pure s
                                

isSingleton :: Foldable f => f Generate -> Subset -> Bool
isSingleton (toList -> generators) subset =
  let counts = count <$> case subset of
                 AllShapes    -> generators
                 SomeShapes s -> filter ((== s) . shape) generators
  in case counts of
       [One] -> True
       _     -> False

randomGenerate :: RVarT m Generate
randomGenerate = Generate <$> boundedEnumStdUniform <*> boundedEnumStdUniform <*> boundedEnumStdUniform

randomAction :: NonEmpty Generate -> RVarT m Action
randomAction generators = do
  let shapes = S.toList . S.fromList . toList $ shape <$> generators
  subset <- randomElementT' $ case shapes of
              [shape] -> SomeShapes shape :| []
              _       -> AllShapes :| (SomeShapes <$> shapes)
  
  targets <- enumUniform (if all isRounded $ subsetShapes generators subset
                          then minForRounded
                          else minForUnrounded)
                         (if isSingleton generators subset
                          then maxForSingular
                          else maxForPlural)
  
  relate  <- boundedEnumStdUniform
  relator <- boundedEnumStdUniform

  pure Action{..}

randomRules :: RVarT m Rules
randomRules = do
  let someNumber rule = flip replicateNonEmptyA rule =<< uniformT 2 4
  generators <- someNumber randomGenerate
  actions    <- someNumber $ randomAction generators
  pure . Rules $ (GenerateRule <$> generators) <> (ActionRule <$> actions)

describeCount :: Count -> String
describeCount One     = "a"
describeCount Two     = "two"
describeCount Three   = "three"
describeCount Four    = "four"
describeCount Five    = "five"
describeCount Several = "several"

describeShape :: Shape -> String
describeShape Circle   = "circle"
describeShape Triangle = "triangle"
describeShape Square   = "square"
describeShape Blob     = "blob"
describeShape Polygon  = "polygon"

describeShapes :: Shape -> String
describeShapes = (++ "s") . describeShape -- Works for these shapes

describeSpacedLocation :: Location -> String
describeSpacedLocation InTheMiddle  = " in the middle of the page"
describeSpacedLocation NearTheEdges = " near the edges of the page"
describeSpacedLocation InTheCorners = " in the corners of the page"
describeSpacedLocation Somewhere    = ""

describeCapitalizedRelate :: Relate -> String
describeCapitalizedRelate Connect  = "Connect"
describeCapitalizedRelate Separate = "Separate"

describeRelator :: Relator -> String
describeRelator StraightLines = "straight lines"
describeRelator Arcs          = "arcs"
describeRelator LoopDeLoops   = "loop-de-loops"
describeRelator Diamonds      = "diamonds"

describeSubset :: Foldable f => f Generate -> Subset -> String
describeSubset generators subset =
  case subset of
    AllShapes    | singular  -> "shape"
                 | otherwise -> "shapes"
    SomeShapes s | singular  -> describeShape s
                 | otherwise -> describeShapes s
  where singular = isSingleton generators subset

describeShapeTargetsSpaced :: (Foldable f, Applicative f) => f Generate -> Subset -> Targets -> String
describeShapeTargetsSpaced _ _ Edges = "edges of the "
describeShapeTargetsSpaced generators subset Corners
  | all isRounded shapes = "extremal points of the "
  | any isRounded shapes = "corners or extremal points of the "
  | otherwise            = "corners of the "
  where shapes = subsetShapes generators subset
describeShapeTargetsSpaced _ _ Shapes  = ""

describeGenerate :: PrintfType t => Generate -> t
describeGenerate Generate{..} =
  printf "Draw %s %s%s." (describeCount          count)
                         ((if count == One then describeShape else describeShapes) shape)
                         (describeSpacedLocation location)

describeAction :: [Generate] -> Action -> String
describeAction generators Action{..} =
  printf "%s the %s%s with %s." (describeCapitalizedRelate                    relate)
                                (describeShapeTargetsSpaced generators subset targets)
                                (describeSubset             generators        subset)
                                (describeRelator                              relator)

describe :: Rules -> String
describe (Rules rules) = unlines . snd . mapAccumL describe1 [] . zip [1 :: Int ..] $ toList rules
  where
    maxListIndexLength = length . show $ length rules
    
    describe1 generators (i, rule) =
      let (addGenerators, description) = case rule of
            GenerateRule g -> ((g:), describeGenerate g)
            ActionRule   a -> (id,   describeAction generators a)
      in (addGenerators generators, printf "%-*d. %s" maxListIndexLength i description)

-- I used this for January 4; can I factor it out?
data Seed = Genuary2021Seed
          | ExplicitSeed !Word64 !Word64
          | RandomSeed
          deriving (Eq, Ord, Show, Read)

genuary2021Seed :: (Word64, Word64)
genuary2021Seed = (12275345074937468904, 5796612794544719813)

getSeed :: Seed -> IO (Word64, Word64)
getSeed Genuary2021Seed            = pure genuary2021Seed
getSeed (ExplicitSeed seed1 seed2) = pure (seed1, seed2)
getSeed RandomSeed                 = (,) <$> sysRandom <*> sysRandom

-- This is more similar to January 3
argumentsParser :: Parser Seed
argumentsParser = pure Genuary2021Seed <|> subcommands
  where
    genuary2021 = info (pure Genuary2021Seed)
                       (progDesc "Generate the rules I used for Genuary 2021 (default)")
    seeded      = info (ExplicitSeed <$> argument auto (metavar "SEED1")
                                     <*> argument auto (metavar "SEED2"))
                       (progDesc "Generate rules with a given seed")
    random      = info (pure RandomSeed)
                       (progDesc "Generate rules with a random seed, and print out that seed")
     
    subcommands = hsubparser $ mconcat [ command "genuary2021" genuary2021
                                       , command "seeded"      seeded
                                       , command "random"      random ]

-- Also similar to January 4
argumentsInfo :: ParserInfo Seed
argumentsInfo = info (argumentsParser <**> helper) (fullDesc <> progDescDoc helpText) where
  helpText = unChunk $ vsepChunks
    [ paragraph "Generate a set of rules that can be followed to draw a picture by hand.  \
                \Without arguments, generates the set of rules I used for Genuary 2021."
    , paragraph "The intended meaning of some of the language is as follows, but since the rules \
                \were designed to leave the human artist some agency, feel free to use your own \
                \interpretation:"
    , fmap (indent 2) . vcatChunks $ map (fmap (hang 2) . paragraph . ("∙ " ++))
        [ "A \"blob\" is a shape with a curvy border."
        , "A \"polygon\" is any convex polygon (triangle, square, pentagon, hexagon, …), \
          \and not necessarily regular."
        , "When asked to \"connect\" or \"separate\" the \"edges\", \"corners\", or \"extremal \
          \points\" of a shape, each shape should have this operation applied to it separately; \
          \no two shapes will be conected or separated."
        , "\"Connecting\" objects can mean transitively or completely: either there can be a \
          \connection from one object to the next to the next, or there must be connections from \
          \each object being connected to every other one."
        , "To \"separate\" objects means to draw the separator in between the offending points, \
          \so you could not traverse from one to the other without crossing a separator."
        , "\"Loop-de-loops\" are intended to be coils, like telephone wire, although there's \
          \ambiguity baked in; think \"⅏\" or \"➿\"."
        , "\"Diamonds\" are intended to be long skinny diamonds, like an expanded line."
        , "The \"extremal points\" of a circle are its four cardinal direction points."
        , "The \"extremal points\" of a blob are where its tangent is zero, but you may have to \
          \use your discretion." ]
    , paragraph "Happy drawing!" ]

-- Also similar to January 4
mainWith :: Seed -> IO ()
mainWith seed = do
  (seed1,seed2) <- getSeed seed
  when (seed == RandomSeed) $
    putStrLn $ "[Seed: " ++ show seed1 ++ " " ++ show seed2 ++ "]"
  
  gen        <- initialize seed1 seed2
  putStr . describe =<< sampleFrom @RVar gen randomRules

main :: IO ()
main = mainWith =<< execParser argumentsInfo
