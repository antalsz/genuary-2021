module Genuary2021.Util.Color (hsv, hsluv, hsluv') where

import Genuary2021.Util.Ord

import Data.Colour.SRGB
import Data.Colour.RGBSpace
import qualified Data.Colour.RGBSpace.HSV as C
import HSLuv

hsv :: (Floating a, RealFrac a)
    => a -- ^ Hue ∈ [0,360] (wrapping)
    -> a -- ^ Saturation ∈ [0,1] (unchecked)
    -> a -- ^ Value ∈ [0,1] (unchecked)
    -> Colour a
hsv h s v = uncurryRGB sRGB $ C.hsv h s v

hsluv :: Double -- ^ Hue ∈ [0,360) (wrapping)
      -> Double -- ^ Saturation ∈ [0,1] (clamped)
      -> Double -- ^ Lightness ∈ [0,1] (clamped)
      -> Colour Double
hsluv h s l = hsluvToColour $ HSLuv (HSLuvHue h)
                                    (HSLuvSaturation $ 100 * clamp 0 1 s)
                                    (HSLuvLightness  $ 100 * clamp 0 1 l)

hsluv' :: Double -- ^ Hue ∈ [0,1] (clamped)
       -> Double -- ^ Saturation ∈ [0,1] (clamped)
       -> Double -- ^ Lightness ∈ [0,1] (clamped)
       -> Colour Double
hsluv' h  = hsluv $ 360 * clamp 0 1 h
             
