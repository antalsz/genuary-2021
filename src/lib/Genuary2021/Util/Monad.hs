{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}

module Genuary2021.Util.Monad (
  tryNTimes, tryNTimesOr,
  replicateDistinct, ReplicationStrategy(..), DistinctnessFailure(..)
) where

import GHC.Generics
import Data.Universe.Class
import Genuary2021.Util.Universe

import Control.Monad
import Control.Monad.Trans
import Control.Monad.Trans.State

import qualified Data.Set as S

tryNTimes :: Monad m => Int -> m (Maybe a) -> m (Maybe a)
tryNTimes n trial = go n where
  go n | n <= 0    = pure Nothing
       | otherwise = maybe (go $ n-1) (pure . Just) =<< trial

tryNTimesOr :: Monad m => Int -> m a -> m (Maybe a) -> m a
tryNTimesOr n fallback = maybe fallback pure <=< tryNTimes n

data ReplicationStrategy = ReplicationStrategy { trials    :: !Int
                                               , onFailure :: !DistinctnessFailure }
                         deriving (Eq, Ord, Generic, Finite, Show, Read)
                         deriving Universe via ViaGUniverse ReplicationStrategy

data DistinctnessFailure = StopShort | GenerateDuplicates
                         deriving (Eq, Ord, Generic, Enum, Bounded, Universe, Finite, Show, Read)

replicateDistinct :: (Monad m, Ord a) => ReplicationStrategy -> Int -> m a -> m [a]
replicateDistinct ReplicationStrategy{..} n₀ action = evalStateT (go n₀) S.empty where
  go n | n <= 0    = pure []
       | otherwise = tryNTimes trials tryFresh >>= \case
                       Just value -> (value :) <$> go (n-1)
                       Nothing    -> case onFailure of
                                       StopShort          -> pure []
                                       GenerateDuplicates -> lift $ replicateM n action
  
  tryFresh = do
    value  <- lift action
    gets (value `S.member`) >>= \case
      True  -> pure Nothing
      False -> Just value <$ modify (S.insert value)
