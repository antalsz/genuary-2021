module Genuary2021.Util.Ord (clamp, atLeast, atMost) where

clamp :: Ord a => a -> a -> a -> a
clamp l h = atLeast l . atMost h
{-# INLINE clamp #-}

-- | A synonym for 'max' that's more clear for some uses: compare @atLeast 0 x@
-- with @0 `max` x@.
atLeast :: Ord a => a -> a -> a
atLeast = max
{-# INLINE atLeast #-}

-- | A synonym for 'min' that's more clear for some uses: compare @atMost 255 x@
-- with @255 `min` x@.
atMost :: Ord a => a -> a -> a
atMost = min
{-# INLINE atMost #-}
