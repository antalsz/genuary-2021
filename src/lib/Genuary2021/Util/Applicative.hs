module Genuary2021.Util.Applicative (
  replicateNonEmptyA
) where

import Control.Monad

import Data.List.NonEmpty (NonEmpty(..))

replicateNonEmptyA :: Applicative f => Int -> f a -> f (NonEmpty a)
replicateNonEmptyA n act = (:|) <$> act <*> replicateM (n-1) act
