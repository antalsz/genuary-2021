{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}

module Genuary2021.Util.Enum (toEnumMaybe) where

toEnumMaybe :: forall a. (Enum a, Bounded a) => Int -> Maybe a
toEnumMaybe i
  | i < fromEnum (minBound @a) = Nothing
  | i > fromEnum (maxBound @a) = Nothing
  | otherwise                  = Just $ toEnum i
