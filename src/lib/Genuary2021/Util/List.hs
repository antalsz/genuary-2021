{-# LANGUAGE LambdaCase #-}

module Genuary2021.Util.List (
  (|++), (++|),
  uncons, unsafeUncons,
  unsnoc, unsafeUnsnoc,
  adjacentPairs,
  english
) where

import Data.Bifunctor
import Data.Maybe
import Data.List.NonEmpty (NonEmpty(..))

(|++) :: NonEmpty a -> [a] -> NonEmpty a
(x :| xs) |++ ys = x :| (xs ++ ys)
infixl 4 |++

(++|) :: [a] -> NonEmpty a -> NonEmpty a
[]       ++| ys        = ys
(x : xs) ++| (y :| ys) = x :| (xs ++ y : ys)
infixr 5 ++|

uncons :: [a] -> Maybe (a, [a])
uncons []     = Nothing
uncons (x:xs) = Just (x,xs)

unsafeUncons :: [a] -> (a, [a])
unsafeUncons = fromMaybe (error "unsafeUncons: empty list") . uncons

unsnoc :: [a] -> Maybe ([a], a)
unsnoc []     = Nothing
unsnoc (x:xs) = Just $ go x xs where
  go x []     = ([], x)
  go x (y:ys) = first (x:) $ go y ys

unsafeUnsnoc :: [a] -> ([a], a)
unsafeUnsnoc = fromMaybe (error "unsafeUnsnoc: empty list") . unsnoc

adjacentPairs :: [a] -> [(a,a)]
adjacentPairs []        = []
adjacentPairs [_]       = []
adjacentPairs (x:y:xys) = (x,y) : adjacentPairs xys

english :: String -> String -> String -> String -> String -> [String] -> String
english nothing what whats punct conj = \case
  []               -> nothing
  [thing]          -> what +@+ thing
  [thing1, thing2] -> whats +@+ thing1 +@+ conj +@+ thing2
  things           -> let (prefix, lastThing) = unsafeUnsnoc things
                      in whats +@+ foldr1 (+@+) (map (++ punct) prefix) +@+ conj +@+ lastThing
  where
    "" +@+ t  = t
    s  +@+ "" = s
    s  +@+ t  = s ++ " " ++ t
    infixr 5 +@+
