{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Genuary2021.Util.Random (
  -- * Non-empty lists
  shuffleT', shuffle', randomElementT', randomElement',
  -- * Zipfian (power law) distributions
  Zipfian(..), filterZipfian, randomZipfianT, randomZipfian, zipfT, zipf,
  -- * Miscellaneous
  chooseMT, chooseM, chooseT, choose,
  possiblyT, possibly
) where

import GHC.Generics

import Data.Function

import Data.List.NonEmpty (NonEmpty(..))
import qualified Data.List.NonEmpty as NEL

import Data.Random
import Data.Random.List
import Data.Random.Distribution.Bernoulli
import Data.Random.Distribution.Categorical

import Data.Urn.MonadSample

instance MonadSample (RVarT m) where
  randomWord = uncurry uniformT

chooseMT :: Double -> RVarT m a -> RVarT m a -> RVarT m a
chooseMT p t f = bernoulliT p >>= \case
                    True  -> t
                    False -> f

chooseM :: Double -> RVar a -> RVar a -> RVar a
chooseM = chooseMT
{-# INLINE chooseM #-}

chooseT :: Double -> a -> a -> RVarT m a
chooseT p = chooseMT p `on` pure
{-# INLINE chooseT #-}

choose :: Double -> a -> a -> RVarT m a
choose = chooseT
{-# INLINE choose #-}

possiblyT :: (Ord p, Fractional p, Distribution StdUniform p)
          => p -> RVarT m a -> RVarT m (Maybe a)
possiblyT p m = bernoulliT p >>= \case
                  True  -> Just <$> m
                  False -> pure Nothing

possibly :: (Ord p, Fractional p, Distribution StdUniform p)
         => p -> RVar a -> RVar (Maybe a)
possibly = possiblyT
{-# INLINE possibly #-}

shuffleT' :: NonEmpty a -> RVarT m (NonEmpty a)
shuffleT' = fmap NEL.fromList . shuffleT . NEL.toList
{-# INLINE shuffleT' #-}

shuffle' :: NonEmpty a -> RVar (NonEmpty a)
shuffle' = shuffleT'
{-# INLINE shuffle' #-}

randomElementT' :: NonEmpty a -> RVarT m a
randomElementT' = randomElementT . NEL.toList
{-# INLINE randomElementT' #-}

randomElement' :: NonEmpty a -> RVar a
randomElement' = randomElementT'
{-# INLINE randomElement' #-}

data Zipfian p a = Zipfian { orderedValues  :: !(NonEmpty a)
                           , frequencyPower :: !p }
                 deriving (Eq, Ord, Generic, Show, Read)

filterZipfian :: (a -> Bool) -> Zipfian p a -> Maybe (Zipfian p a)
filterZipfian p zipfian = do
  orderedValues' <- NEL.nonEmpty . NEL.filter p $ orderedValues zipfian
  pure zipfian{orderedValues = orderedValues'}

randomZipfianT :: p -> NonEmpty a -> RVarT m (Zipfian p a)
randomZipfianT a = fmap (flip Zipfian a) . shuffleT'

randomZipfian :: p -> NonEmpty a -> RVar (Zipfian p a)
randomZipfian = randomZipfianT
{-# INLINE randomZipfian #-}

zipfT :: (Ord p, Floating p, Distribution Uniform p) => Zipfian p a -> RVarT m a
zipfT (Zipfian xs a) = categoricalT . zip (map ((** (-a)) . fromInteger) [1..]) $ NEL.toList xs

zipf :: (Ord p, Floating p, Distribution Uniform p) => Zipfian p a -> RVar a
zipf = zipfT
{-# INLINE zipf #-}
