{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

{-# OPTIONS_GHC -Wno-orphans #-}

module Genuary2021.Util.Random.Source.PCG (
  -- * Re-export "System.Random.PCG" with some instances for random-fu, except
  -- that we generalize all references to 'IO'
  module System.Random.PCG,
  -- * Generalize all references to 'IO'
  withSystemRandom, createSystemRandom,
  -- * Convenience function for locally running within a 'MonadRandom'
  runWithSystemRandom
) where

import Control.Monad.Primitive
import Control.Monad.Trans.Reader
import Data.Random.Source
import System.Random.PCG hiding (withSystemRandom, createSystemRandom)
import System.Random.PCG.Class

$(randomSource [d|
    instance (PrimMonad m, PrimState m ~ s) => RandomSource m (Gen s) where
      getRandomWord8From  = uniformW8
      getRandomWord16From = uniformW16
      getRandomWord32From = uniformW32
      getRandomWord64From = uniformW64
      getRandomDoubleFrom = uniformB 1
      {-# INLINE getRandomWord8From  #-}
      {-# INLINE getRandomWord16From #-}
      {-# INLINE getRandomWord32From #-}
      {-# INLINE getRandomWord64From #-}
      {-# INLINE getRandomDoubleFrom #-}
  |])

$(monadRandom [d|
  instance (PrimMonad m, PrimState m ~ s) => MonadRandom (ReaderT (Gen s) m) where
      getRandomWord8          = getRandomWord8From               =<< ask
      getRandomWord16         = getRandomWord16From              =<< ask
      getRandomWord32         = getRandomWord32From              =<< ask
      getRandomWord64         = getRandomWord64From              =<< ask
      getRandomDouble         = getRandomDoubleFrom              =<< ask
      getRandomNByteInteger n = flip getRandomNByteIntegerFrom n =<< ask
      {-# INLINE getRandomWord8        #-}
      {-# INLINE getRandomWord16       #-}
      {-# INLINE getRandomWord32       #-}
      {-# INLINE getRandomWord64       #-}
      {-# INLINE getRandomDouble       #-}
      {-# INLINE getRandomNByteInteger #-}
  |])

-- -- By reimplementing this, we avoid the need for 'MonadBaseControl'
withSystemRandom :: (PrimMonad m, PrimState m ~ RealWorld) => (GenIO -> m a) -> m a
withSystemRandom f = f =<< ioToPrim do
  w1 <- sysRandom
  w2 <- sysRandom
  initialize w1 w2

createSystemRandom :: (PrimMonad m, PrimState m ~ RealWorld) => m GenIO
createSystemRandom = withSystemRandom pure

runWithSystemRandom :: (PrimMonad m, PrimState m ~ RealWorld) => ReaderT GenIO m a -> m a
runWithSystemRandom act = withSystemRandom $ runReaderT act
