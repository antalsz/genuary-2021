{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module Genuary2021.Util.Image (
  -- * Generate an image by applying a function at every pixel
  image,
  -- * Convert to 'Pixel's
  -- ** In general
  ToPixel(..),
  -- ** Colors specifically
  ColorPixel(..), ColoredPixel,
  -- * Using a different indexing scheme for the pixels of an image
  ReindexPixels,
  -- ** Cartesian coordinates
  cartesianInt,
  cartesian01Incl, cartesian01Excl,
  cartesianBiunitIncl, cartesianBiunitExcl,
  -- ** Polar coordinates
  polar, polarNegPi,
  polar01Incl, polar01Excl,
  -- * Mutable images
  regenerateMutableImage, unsafeRegenerateMutableImage
) where

import Data.Kind

import Genuary2021.Util.Ord
import Genuary2021.Util.Function
import Control.Monad.Primitive

import qualified Data.Vector.Storable.Mutable as VSM

import Data.Colour
import Data.Colour.RGBSpace
import Data.Colour.SRGB

import Codec.Picture
import Codec.Picture.Types

--------------------------------------------------------------------------------
-- Reindexing

-- |A value of type @ReindexPixels a@ allows you to work in an environment
-- expecting a discrete Cartesian indexing schemes while using another indexing
-- scheme.  The canonical example is working with pixels in an image: by
-- default, the pixels in a /w/×/h/ image are located at the points
-- [0,1../w/-1]×[0,1../h/-1].  However, you might instead want to imagine them
-- located on the half-open unit square [0,1)².  A value of type @ReindexPixels
-- Double@ would let you write the expected @Double -> Double -> px@ function,
-- and then use it where the @Int -> Int -> px@ function was expected.  To use a
-- value @reindexer@ of this type, simply apply it to the width, the height, and
-- your function that computes values (pixels) based on a new indexing strategy.
--
-- To use this function, see 'generateImageOver' and 'generateColorImageOver'.
type ReindexPixels a = forall r. Int -> Int -> (a -> a -> r) -> (Int -> Int -> r)

--------------------------------------------------------------------------------
-- Cartesian

-- Local
for01Excl, for01Incl :: (Integral a, Num b) => a -> b
for01Excl x = fromIntegral x
for01Incl x = fromIntegral . atLeast 1 . subtract 1 $ x
{-# INLINE for01Excl #-}
{-# INLINE for01Incl #-}

-- Local
cartesian01By :: Fractional a => (Int -> a) -> ReindexPixels a
cartesian01By upperBound = \width height pixel ->
  let widthUpper  = upperBound width
      heightUpper = upperBound height
  in \x y -> pixel (fromIntegral x / widthUpper) (fromIntegral y / heightUpper)
{-# INLINE cartesian01By #-}

-- |The identity pixel reindexer; used when you want to write functions which
-- assume pixels are located at discrete Cartesian points,
-- [0../width/-1]×[0../height/-1].
cartesianInt :: ReindexPixels Int
cartesianInt _ _ = id
{-# INLINE cartesianInt #-}

-- |Remap the pixels to lie in the half-open unit square [0,1)².
cartesian01Excl :: Fractional a => ReindexPixels a
cartesian01Excl = cartesian01By for01Excl
{-# INLINE cartesian01Excl #-}

-- |Remap the pixels to lie in the (closed) unit square [0,1]².
cartesian01Incl :: Fractional a => ReindexPixels a
cartesian01Incl = cartesian01By for01Incl
{-# INLINE cartesian01Incl #-}

-- Local
cartesianBiunitBy :: Fractional a => (Int -> Int) -> (Int -> a) -> ReindexPixels a
cartesianBiunitBy adjust upperBound = \width height pixel ->
  let widthMid  = upperBound (adjust width)  / 2
      heightMid = upperBound (adjust height) / 2
  in \x y -> pixel ((fromIntegral (adjust x) - widthMid)  / widthMid)
                   ((fromIntegral (adjust y) - heightMid) / heightMid)
{-# INLINE cartesianBiunitBy #-}

-- |Remap the pixels to line in the open biunit square (0,1)²
cartesianBiunitExcl :: Fractional a => ReindexPixels a
cartesianBiunitExcl = cartesianBiunitBy (+1) for01Excl
{-# INLINE cartesianBiunitExcl #-}

-- |Remap the pixels to line in the (closed) biunit square [0,1]²
cartesianBiunitIncl :: Fractional a => ReindexPixels a
cartesianBiunitIncl = cartesianBiunitBy id for01Incl
{-# INLINE cartesianBiunitIncl #-}

-- TODO: These currently forcibly map everything onto the square, changing the
-- aspect ratio.  Should they instead make the largest dimension 1?  The
-- smallest?  Should we provide all of these?  If so, what's the default?

--------------------------------------------------------------------------------
-- Polar

-- Local
polarBy :: RealFloat a => (a -> a -> a) -> ReindexPixels a
polarBy computeAngle = \width height pixel ->
  let midX = fromIntegral width  / 2
      midY = fromIntegral height / 2
  in \x y -> let x' = fromIntegral x - midX
                 y' = fromIntegral y - midY
             in pixel (sqrt $ x'*x' + y'*y') (computeAngle y' x')
{-# INLINE polarBy #-}

-- |Like 'polar', except that θ ∈ (-pi,pi].
polarNegPi :: RealFloat a => ReindexPixels a
polarNegPi = polarBy atan2
{-# INLINABLE polarNegPi #-}

-- |Address the pixels with polar coordinates (/r/,θ).  The origin lies in the
-- center of the image (possibly between pixels).  The value of /r/ is computed
-- based on the original discrete Cartesian coordinates.  The angle θ ∈ [0,2π)
-- is measured in radians; at the center of the image, θ = 0.
polar :: RealFloat a => ReindexPixels a
polar = polarBy \y x -> atan2 (-y) (-x) + pi
        -- The trick here to push 'atan2' into [0,2*pi) without branching is due
        -- to aib's answer to willc2's question "How to map atan2() to degrees
        -- 0-360" on Stack Overflow:
        -- <https://stackoverflow.com/a/1311572/237428>
{-# INLINABLE polar #-}

-- Local
polar01By :: RealFloat a => (Int -> a) -> ReindexPixels a
polar01By upperBound = \width height pixel ->
  let width'  = upperBound width
      height' = upperBound height
      maxDist = sqrt (width'*width' + height'*height') / 2
  in polar width height $ pixel . (/maxDist)
{-# INLINABLE polar01By #-}

-- |Like 'polar', but /r/ is normalized to lie in [0,1) (attaining its
-- maximum value at the corners).
polar01Excl :: RealFloat a => ReindexPixels a
polar01Excl = polar01By for01Excl
{-# INLINE polar01Excl #-}

-- |Like 'polar', but /r/ is normalized to lie in [0,1] (attaining its
-- maximum value at the corners).
polar01Incl :: RealFloat a => ReindexPixels a
polar01Incl = polar01By for01Incl
{-# INLINE polar01Incl #-}

--------------------------------------------------------------------------------
-- Color-to-pixel conversion

-- |The type class for 'Pixel's that can be losslessly (up to precision)
-- produced from a 'Colour'.
class Pixel px => ColorPixel px where
  -- |The necessary type constraints to do the coercion
  type ColorPixelConstraints px a :: Constraint
  -- |Convert a 'Colour' to a 'Pixel'
  colorPixel :: ColorPixelConstraints px a => Colour a -> px

-- |A convenient abbreviated constraint when working with polymorphic 'Colour's
type ColoredPixel px a = (ColorPixel px, ColorPixelConstraints px a)

instance ColorPixel PixelRGB8 where
  type ColorPixelConstraints PixelRGB8 a = (RealFrac a, Floating a)
  colorPixel = uncurryRGB PixelRGB8 . toSRGBBounded
  {-# INLINE colorPixel #-}

instance ColorPixel PixelRGB16 where
  type ColorPixelConstraints PixelRGB16 a = (RealFrac a, Floating a)
  colorPixel = uncurryRGB PixelRGB16 . toSRGBBounded
  {-# INLINE colorPixel #-}

instance ColorPixel PixelRGBF where
  type ColorPixelConstraints PixelRGBF a = Real a
  colorPixel = uncurryRGB PixelRGBF . toSRGB . colourConvert
  {-# INLINE colorPixel #-}

-- Also: 'PixelCMYK8', 'PixelCMYK16', 'PixelYCbCr8', 'PixelYCbCrK8'

--------------------------------------------------------------------------------
-- Conversion into pixels

-- Not sure how to handle 'AlphaColour'; the "colour" library doesn't seem to
-- provide the right primitives

class Pixel px => ToPixel c px where
  toPixel :: c -> px

-- Pixel identity instances
instance ToPixel Pixel8       Pixel8       where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel Pixel16      Pixel16      where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel Pixel32      Pixel32      where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelF       PixelF       where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelRGB8    PixelRGB8    where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelRGB16   PixelRGB16   where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelRGBF    PixelRGBF    where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelCMYK8   PixelCMYK8   where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelCMYK16  PixelCMYK16  where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelYCbCr8  PixelYCbCr8  where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelYCbCrK8 PixelYCbCrK8 where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelYA8     PixelYA8     where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelYA16    PixelYA16    where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelRGBA8   PixelRGBA8   where toPixel = id ; {-# INLINE toPixel #-}
instance ToPixel PixelRGBA16  PixelRGBA16  where toPixel = id ; {-# INLINE toPixel #-}

instance ColoredPixel px a => ToPixel (Colour a) px where
  toPixel = colorPixel
  {-# INLINE toPixel #-}

--------------------------------------------------------------------------------
-- Generators

-- |Like 'image', but more flexible.  First, the pixel generating function lives
-- in a different coordinate system, which is defined by the @'ReindexPixels' a@
-- mapping.  Second, the supplied function doesn't have to generate 'Pixel's,
-- but can instead generate anything that can be converted into a pixel via the
-- 'ToPixel' type class.
image :: forall px c a.
         ToPixel c px
      => ReindexPixels a -> (a -> a -> c) -> Int -> Int -> Image px
image reindexer pixel width height =
  generateImage (toPixel .: reindexer width height pixel) width height
{-# INLINE image #-}

-- |Given a 'MutableImage', use the specified pixel-generating function and
-- pixel indexing scheme to overwrite every pixel of it.  This function is
-- /unsafe/, as it does no bounds checking.  The unsafe destructive mutable
-- analog of 'image'.
--
-- The implementation is largely copied from the internal function
-- 'Codec.Picture.Types.generateMutableImage' in JuicyPixels.
unsafeRegenerateMutableImage :: forall px c a m.
                                (PrimMonad m, ToPixel c px)
                             => ReindexPixels a               -- ^ Pixel indexing scheme
                             -> (a -> a -> m c)              -- ^ Pixel generating function
                             -> MutableImage (PrimState m) px -- ^ The image to overwrite
                             -> m ()
unsafeRegenerateMutableImage reindex pixel MutableImage{ mutableImageWidth  = width
                                                       , mutableImageHeight = height
                                                       , mutableImageData   = pixelVec } =
  lineGenerator 0 0
  where
    lineGenerator lineIdx y
      | y >= height = pure ()
      | otherwise   = column lineIdx 0
      where column idx x
              | x >= width = lineGenerator idx $ y + 1
              | otherwise  = do
                  unsafeWritePixel @px pixelVec idx . toPixel =<< reindex width height pixel x y
                  column (idx + compCount) $ x + 1
    
    compCount = componentCount @px undefined
{-# INLINE unsafeRegenerateMutableImage #-}

-- |Given a 'MutableImage', use the specified pixel-generating function and
-- pixel indexing scheme to overwrite every pixel of it.  The destructive
-- mutable analog of 'image'.
regenerateMutableImage :: forall px c a m.
                          (PrimMonad m, ToPixel c px)
                       => ReindexPixels a               -- ^ Pixel indexing scheme
                       -> (a -> a -> m c)                -- ^ Pixel generating function
                       -> MutableImage (PrimState m) px -- ^ The image to overwrite
                       -> m ()
regenerateMutableImage reindex pixel img@MutableImage{..}
  | VSM.length mutableImageData == mutableImageWidth * mutableImageHeight * componentCount @px undefined =
    unsafeRegenerateMutableImage reindex pixel img
  | otherwise =
    error "regenerateMutableImage: Invalid MutableImage; the data is the wrong size"
{-# INLINE regenerateMutableImage #-}
