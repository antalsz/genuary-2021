{-# LANGUAGE ConstraintKinds #-}
{-# LANGUAGE TypeFamilies #-}

module Genuary2021.Util.Primitive (
  PrimIOMonad
) where

import Control.Monad.Primitive

type PrimIOMonad m = (PrimMonad m, PrimState m ~ RealWorld)
