{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module Genuary2021.Util.NASA.Fireball (
  -- * Types
  Fireball(..),
  -- ** Units
  Latitude, Longitude,
  Kilometers, KilometersPerSecond,
  TenGigajoules, Kilotons,
  -- * API query
  -- ** Run
  getFireballs, getFireballsResponse,
  -- ** Types
  Query(..), QueryRange(..), Sort(..), SortDirection(..), SortField(..),
  defaultQuery, nasaDefaultQuery, queryOptions,
  -- * Parsing
  parseFireballs, Fireballs(..)
) where

-- Parsing NASA JSON

import Control.Lens

import Data.Coerce

import Data.Maybe
import Data.Either
import Data.These
import Data.Align
import Data.Foldable
import Control.Monad

import Data.Char
import qualified Text.ParserCombinators.ReadP as ReadP
import Data.Text (Text())
import qualified Data.Text as T
import qualified Data.ByteString.Lazy as LBS

import Data.List ((\\), intercalate)
import Data.Vector (Vector())
import qualified Data.Vector as V
import Linear.V3

import Data.Scientific
import Data.HashMap.Strict (HashMap())
import qualified Data.HashMap.Strict as HM

import Data.Time
import Data.Aeson hiding (Options(..))
import Data.Aeson.Types hiding (Options(..))

import Network.Wreq

--------------------------------------------------------------------------------
-- Fireball data

{- Fireballs are bright meteors seen from earth:

   > /Fireballs/ and /bolides/ are astronomical terms for exceptionally bright
   > meteors that are spectacular enough to to [sic] be seen over a very wide
   > area.
   >
   > […]
   >
   > A /fireball/ is an unusually bright meteor that reaches a visual magnitude
   > of -3 or brighter when seen at the observer’s zenith.  Objects causing
   > fireball events can exceed one meter in size. Fireballs that explode in the
   > atmosphere are technically referred to as /bolides/ although the terms
   > fireballs and bolides are often used interchangeably.
   
   (From "Fireballs (Introduction)", Center for Near Earth Object Studies,
   <https://cneos.jpl.nasa.gov/fireballs/intro.html>)
   
   NASA/JPL/CNEOS (National Aeronautics and Space Administration/Jet Propulsion
   Laboratory/Center for Near Earth Objec Studies) provides a database of
   fireball events, accessible via a JSON API.
   
   * Home page: <https://cneos.jpl.nasa.gov/fireballs/>
   * Introduction: <https://cneos.jpl.nasa.gov/fireballs/intro.html>
   * API documentation: <https://ssd-api.jpl.nasa.gov/doc/fireball.html>
   * Master list of NASA APIs: <https://api.nasa.gov>
-}

data Fireball = Fireball
  { dateTime         :: !UTCTime                          -- ^Date/time of peak brightness (technically GMT)
  , location         :: !(Maybe (Latitude,Longitude))     -- ^Latitude and longitude at peak brightness
  , altitude         :: !(Maybe Kilometers)               -- ^Altitude at peak brightness (nonnegative)
  , velocity         :: !(Maybe KilometersPerSecond)      -- ^Velocity at peak brightness (nonnegative)
  , energy           :: !TenGigajoules                    -- ^Approximate total radiated energy
  , impactEnergy     :: !Kilotons                         -- ^Approximate total impact energy
  , preEntryVelocity :: !(Maybe (V3 KilometersPerSecond)) -- ^Pre-entry estimated Earth centered velocity
  } deriving (Eq, Ord, Show, Read)

----------------------------------------
-- Units

-- |Positive = north, negative = south
newtype Latitude  = Latitude  { latitudeDegrees  :: Double } deriving (Eq, Ord, Show, Read)

-- |Positive = east, negative = west
newtype Longitude = Longitude { longitudeDegrees :: Double } deriving (Eq, Ord, Show, Read)

-- I could use the @units@ package instead, but that's a heavyweight dependency
-- and doesn't let me put the units in the type.

newtype Kilometers = Kilometers { getKilometers :: Double }
                   deriving (Eq, Ord, Show, Read)

newtype KilometersPerSecond = KilometersPerSecond { getKilometersPerSecond :: Double }
                            deriving (Eq, Ord, Show, Read)

-- |Units of 10¹⁰ J = 10 GJ
newtype TenGigajoules = TenGigajoules { getTenGigajoules :: Double }
                      deriving (Eq, Ord, Show, Read)

newtype Kilotons = Kilotons { getKilotons :: Double }
                 deriving (Eq, Ord, Show, Read)

--------------------------------------------------------------------------------
-- Network query

data QueryRange a = QueryRange { qrMin, qrMax :: !(Maybe a) }
                  deriving (Eq, Ord, Show, Read)

emptyRange :: QueryRange a
emptyRange = QueryRange { qrMin = Nothing, qrMax = Nothing }

data SortDirection = Ascending | Descending deriving (Eq, Ord, Enum, Bounded, Show, Read)

data SortField = DateTime | Energy | ImpactEnergy | Velocity | Altitude
               deriving (Eq, Ord, Enum, Bounded, Show, Read)

data Sort = Sort { sortDirection :: !SortDirection
                 , sortField     :: !SortField }
          deriving (Eq, Ord, Show, Read)

-- |From <https://ssd-api.jpl.nasa.gov/doc/fireball.html#query-parameters>
data Query = Query { dateRange               :: !(QueryRange UTCTime)
                   , energyRange             :: !(QueryRange TenGigajoules)
                   , impactEnergyRange       :: !(QueryRange Kilotons)
                   , velocityRange           :: !(QueryRange KilometersPerSecond)
                   -- , altitudeRange           :: !(QueryRange Kilometers)
                   -- This produces a 500 error
                   , requireLocation         :: !Bool
                   , requireAltitude         :: !Bool
                   , requireVelocity         :: !Bool -- Doesn't work quite right; can still have "-"
                   , requirePreEntryVelocity :: !Bool
                   , includePreEntryVelocity :: !Bool
                   , querySort               :: !Sort
                   , limit                   :: !(Maybe Int) }
           deriving (Eq, Ord, Show, Read)

-- |The defaults are those from
-- <https://ssd-api.jpl.nasa.gov/doc/fireball.html#query-parameters>, except
-- that we include the three-dimensional pre-entry velocity
defaultQuery :: Query
defaultQuery = Query { dateRange               = emptyRange
                     , energyRange             = emptyRange
                     , impactEnergyRange       = emptyRange
                     , velocityRange           = emptyRange
                     -- , altitudeRange           = emptyRange
                     , requireLocation         = False
                     , requireAltitude         = False
                     , requireVelocity         = False
                     , requirePreEntryVelocity = False
                     , includePreEntryVelocity = True
                     , querySort               = Sort { sortDirection = Descending
                                                      , sortField     = DateTime }
                     , limit                   = Nothing }

-- |The true NASA/JPL defaults
nasaDefaultQuery :: Query
nasaDefaultQuery = defaultQuery{includePreEntryVelocity=False}

queryOptions :: Query -> Options
queryOptions Query{..} = defaults & range   "date"        dateRange         timestamp
                                  & range   "energy"      energyRange       wrappedDouble
                                  & range   "impact-e"    impactEnergyRange wrappedDouble
                                  & range   "vel"         velocityRange     wrappedDouble
                                  -- & range   "alt"         altitudeRange     wrappedDouble
                                  & require "loc"         requireLocation
                                  & require "alt"         requireAltitude
                                  & require "vel"         requireVelocity
                                  & require "vel-comp"    requirePreEntryVelocity
                                  & param   "vel-comp" .! bool includePreEntryVelocity
                                  & param   "sort"     .! sort querySort
                                  & param   "limit"    .? (tshow <$> limit)
  where
    l .! x = l .~ [x]
    l .? m = l .~ maybeToList m
    infixr 4 .!, .?
    
    tshow         :: Show a => a -> Text
    timestamp     :: UTCTime -> Text
    wrappedDouble :: Coercible a Double => a -> Text
    
    tshow         = T.pack . show
    timestamp     = T.pack . formatTime defaultTimeLocale "%FT%T"
    wrappedDouble = tshow @Double . coerce
    
    bool True  = "true"
    bool False = "false"
    
    sort Sort{..} = (<>) (case sortDirection of
                            Ascending  -> ""
                            Descending -> "-")
                         (case sortField of
                            DateTime     -> "date"
                            Energy       -> "energy"
                            ImpactEnergy -> "impact-e"
                            Velocity     -> "vel"
                            Altitude     -> "alt")
    
    range name QueryRange{..} text = (param (name <> "-min") .? (text <$> qrMin))
                                   . (param (name <> "-max") .? (text <$> qrMax))
    
    require name b = param ("req-" <> name) .~ [bool b]

-- This service doesn't seem to take an API key
getFireballsResponse :: Query -> IO (Response LBS.ByteString)
getFireballsResponse query =
  getWith (queryOptions query) "https://ssd-api.jpl.nasa.gov/fireball.api"

getFireballs :: Query -> IO (Vector Fireball)
getFireballs query = do
  response <- getFireballsResponse query
  unless (response^.responseStatus.statusCode == 200) $
    fail $ "Could not load data from NASA/JPL Fireball data API endpoint"
  json <- asJSON response
  pure $ json^.responseBody.to unFireballs

--------------------------------------------------------------------------------
-- Parsing

-- Fields, from the API documentation (<https://ssd-api.jpl.nasa.gov/doc/fireball.html>):
--
-- > Field       Description
-- > ───────────────────────────────────────────────────────────────────────────
-- > date        date/time of peak brightness (GMT)
-- > lat         latitude at peak brightness (degrees)
-- > lon         longitude at peak brightness (degrees)
-- > lat-dir     latitude direction (“N” or “S”)
-- > lon-dir     latitude direction (“E” or “W”)
-- > alt         altitude above the geoid at peak brightness (km)
-- > vel         velocity at peak brightness (km/s)
-- > energy      approximate total radiated energy (10¹⁰ joules)
-- > impact-e    approximate total impact energy (kt)
-- > vx          pre-entry estimated velocity (Earth centered X component, km/s)
-- > vy          pre-entry est. velocity (Earth centered Y component, km/s)
-- > vz          pre-entry est. velocity (Earth centered Z component, km/s)
--
-- The only required fields are "date", "energy", and "impact-e"; there is also
-- an explicit guarantee that either all four location fields "lat", "lon",
-- "lat-dir", and "lon-dir" are defined or none of them are, and an implicit
-- guarantee that either all three directed velocity fields "vx", "vy", and "vz"
-- are defined or none of them are.  I also believe that "vel" is always
-- nonnegative, in addition to the altitude and energy fields.
--
-- The correspondences with fields in the 'Fireball' type are as follows:
--
-- > 'Fireball'          API
-- > ────────────────────────────────
-- > dateTime            date
-- > location            lat, lat-dir, lon, lon-dir
-- > altitude            alt
-- > velocity            vel
-- > energy              energy
-- > impactEnergy        impact-e
-- > preEntryVelocity    vx, vy, vz
--
-- The /geoid/ roughly means the surface of the earth

-- Used only during parsing

data LatDir = N | S deriving (Eq, Ord, Enum, Bounded, Show, Read)
data LonDir = E | W deriving (Eq, Ord, Enum, Bounded, Show, Read)

showFields :: [Text] -> String
showFields = intercalate ", " . map show

guardNoSuchFields :: MonadFail m => String -> [Text] -> m ()
guardNoSuchFields msg fields =
  unless (null fields) . fail $ msg ++ ": " ++ showFields fields

parseArray :: (Value -> Parser a) -> Array -> Parser (Vector a)
parseArray p = traverse (uncurry $ parseIndexedJSON p) . V.indexed

withField' :: String -> ((Text -> Parser a) -> Text -> Parser r) -> (Parser e -> Parser r)
           -> String -> (Text -> Parser a) -> Value
           -> Parser r
withField' expectedTypes whenString whenNull name f v =
  let wrongType = typeMismatch expectedTypes v
  in prependFailure ("parsing " ++ show name ++ " field failed, ") case v of
       String s -> whenString f s
       Null     -> whenNull wrongType
       _        -> wrongType

withRequiredField :: String -> (Text -> Parser a) -> Value -> Parser a
withRequiredField = withField' "String" id id

-- Optional: omitted (not handled here), null, or "-"
withOptionalField :: String -> (Text -> Parser a) -> Value -> Parser (Maybe a)
withOptionalField = withField' "String or Null"
                               (\f -> \case "-" -> pure Nothing
                                            s   -> Just <$> f s)
                               (const $ pure Nothing)

readNumberIf :: (MonadFail m, RealFloat a) => (Scientific -> Bool) -> String -> String -> Text -> m a
readNumberIf pred predFailed what t =
  case ReadP.readP_to_S (scientificP <* ReadP.eof) $ T.unpack t of
    [(n,"")] -> do unless (pred n) $ failThis predFailed
                   either (const $ failThis "out of range") pure $ toBoundedRealFloat n
    _        -> failThis "is not a number"
  where failThis msg = fail $ what ++ " " ++ msg

readBoundedNonnegative :: (MonadFail m, RealFloat a) => String -> Scientific -> Text -> m a
readBoundedNonnegative what h =
  readNumberIf (\n -> 0 <= n && n <= h) ("out of range [0," ++ show h ++ "]") what

readNonnegative :: (MonadFail m, RealFloat a) => String -> Text -> m a
readNonnegative = readNumberIf (0 <=) "is negative"

readNumber :: (MonadFail m, RealFloat a) => String -> Text -> m a
readNumber = readNumberIf (const True) ""

readFromHashMap :: MonadFail m => String -> HashMap Text v -> Text -> m v
readFromHashMap what hm k = maybe (fail errmsg) pure $ HM.lookup k hm
  where errmsg = what ++ " must be " ++ intercalate " or " (map show $ HM.keys hm)

allFields :: [Text]
allFields = ["date", "lat", "lon", "lat-dir", "lon-dir", "alt", "vel", "energy", "impact-e", "vx", "vy", "vz"]

mandatoryFields :: [Text]
mandatoryFields = ["date", "energy", "impact-e"]

groupedFields :: [[Text]]
groupedFields = [["lat", "lon", "lat-dir", "lon-dir"], ["vx", "vy", "vz"]]

parseVersion :: Value -> Parser ()
parseVersion = withText "version" \version ->
  unless (version == "1.0") .
    fail $ "unknown version " ++ show version

parseSignature :: Value -> Parser ()
parseSignature = withObject "signature" \sig ->
  explicitParseField parseVersion sig "version"

parseFieldNames :: Value -> Parser [Text]
parseFieldNames = withArray "field names" \fs -> do
  fields <- toList <$> parseArray (withText "field name" pure) fs
  
  guardNoSuchFields "unknown fields"           $ fields          \\ allFields
  guardNoSuchFields "missing mandatory fields" $ mandatoryFields \\ fields
  for_ groupedFields \group -> do
    case group \\ fields of
      missing@(_:_) | length missing /= length group ->
        fail $  "bad set of fields: " ++ showFields (group \\ missing) ++ " are present but "
             ++ showFields missing ++ " are missing"
      _ ->
        pure () -- OK, either all are present or all are absent
  
  pure fields

-- NASA's JSON does not lean into the fact that JSON is a structured format
parseCount :: Value -> Parser Scientific
parseCount = \case
  Number n -> pure n
  String s
    | T.all isDigit s -> pure . read $ T.unpack s
    | otherwise       -> failing $ fail "expected this string to contain only digits"
  v -> failing $ typeMismatch "Number or String" v
  where
    failing = prependFailure "parsing count failed, "

parseRecord :: [Text] -> Value -> Parser Fireball
parseRecord fields = withArray "record" \arr -> do
  let (missing, extra, matched) = partitionThese . align fields $ toList arr
  -- At most of one of @missing@ and @extra@ is nonnull
  guardNoSuchFields "missing" missing
  unless (null extra) . fail $ show (length extra) ++ " extra field(s)"

  let valueRecord = HM.fromList matched
      recordFieldWith with whenMissing field parse =
        maybe whenMissing (with (T.unpack field) parse) $ HM.lookup field valueRecord
      
      recordField field =
        fmap (field,) . recordFieldWith withOptionalField (pure Nothing) field
      recordField' field =
        recordFieldWith withRequiredField (fail $ "missing required field " ++ show field) field
        -- The 'fail' should never be called due to earlier validation
      
  -- Better error parsing would save all the failures, but I can't be bothered
  dateTime     <- recordField' "date"     $ parseTimeM False defaultTimeLocale "%F %T" . T.unpack
  fieldLat     <- recordField  "lat"      $ readBoundedNonnegative "latitude"  90
  fieldLon     <- recordField  "lon"      $ readBoundedNonnegative "longitude" 180
  fieldLatDir  <- recordField  "lat-dir"  . readFromHashMap "latitude"  $ HM.fromList [("N",N),("S",S)]
  fieldLonDir  <- recordField  "lon-dir"  . readFromHashMap "longitude" $ HM.fromList [("E",E),("W",W)]
  (_,altitude) <- recordField  "alt"      $ fmap Kilometers          . readNonnegative "altitude"
  (_,velocity) <- recordField  "vel"      $ fmap KilometersPerSecond . readNonnegative "velocity"
  energy       <- recordField' "energy"   $ fmap TenGigajoules       . readNonnegative "energy"
  impactEnergy <- recordField' "impact-e" $ fmap Kilotons            . readNonnegative "impact energy"
  fieldVx      <- recordField  "vx"       $ fmap KilometersPerSecond . readNumber "x velocity"
  fieldVy      <- recordField  "vy"       $ fmap KilometersPerSecond . readNumber "y velocity"
  fieldVz      <- recordField  "vz"       $ fmap KilometersPerSecond . readNumber "z velocity"
  
  let groupedName (f, Just _)  = Left  f
      groupedName (f, Nothing) = Right f
      
      failGroup what fields =
        let (present, missing) = partitionEithers fields
        in fail $  "malformed " ++ what
                ++ ": found " ++ showFields present
                ++ " but not " ++ showFields missing
  
  location <- case (fieldLat, fieldLon, fieldLatDir, fieldLonDir) of
    ((_, Just lat), (_, Just lon), (_, Just latDir), (_, Just lonDir)) ->
      pure $ Just ( Latitude  $ (case latDir of N -> id ; S -> negate) lat
                  , Longitude $ (case lonDir of E -> id ; W -> negate) lon )
    ((_, Nothing), (_, Nothing), (_, Nothing), (_, Nothing)) ->
      pure Nothing
    (groupedName -> lat, groupedName -> lon, groupedName -> latDir, groupedName -> lonDir) ->
      failGroup "position" [lat, lon, latDir, lonDir]
  
  preEntryVelocity <- case (fieldVx, fieldVy, fieldVz) of
    ((_, Just vx), (_, Just vy), (_, Just vz)) ->
      pure . Just $ V3 vx vy vz
    ((_, Nothing), (_, Nothing), (_, Nothing)) ->
      pure Nothing
    (groupedName -> vx, groupedName -> vy, groupedName -> vz) ->
      failGroup "pre-entry velocity" [vx, vy, vz]
  
  pure Fireball{..}

parseRecords :: [Text] -> Scientific -> Value -> Parser (Vector Fireball)
parseRecords fields count = withArray "records" \arr -> do
  let numRecords = V.length arr
  unless (count == fromIntegral numRecords) $
    fail $  "incorrect record count: count said " ++ show count
         ++ " record(s), but there were actually " ++ show numRecords
  parseArray (parseRecord fields) arr

parseFireballs :: Value -> Parser (Vector Fireball)
parseFireballs = withObject "NASA/JPL fireball data" \o -> do
  guardNoSuchFields "unknown keys" $ HM.keys o \\ ["signature", "count", "fields", "data"]
  
  ()      <- explicitParseField parseSignature              o "signature"
  count   <- explicitParseField parseCount                  o "count"
  if count == 0
  then pure mempty
  else do
    fields  <- explicitParseField parseFieldNames             o "fields"
    records <- explicitParseField (parseRecords fields count) o "data"
    pure records

-- |This type just exists to hang a 'FromJson' instance on
newtype Fireballs = Fireballs { unFireballs :: (Vector Fireball) }

instance FromJSON Fireballs where
  parseJSON = coerce parseFireballs

