{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Genuary2021.Util.Universe (
  -- * Generic version of 'UniverseF'
  GFinite(..), universeFGeneric,
  -- * @DerivingVia@ newtype wrappers
  ViaGUniverse(..), ViaGFinite(..)
) where

import GHC.Generics
import Data.Coerce
import Data.Universe.Class
import Data.Universe.Generic

class GFinite f where
  guniverseF :: [f a]

instance GFinite V1 where
  guniverseF = []

instance GFinite U1 where
  guniverseF = [U1]

instance (GFinite f, GFinite g) => GFinite (f :+: g) where
  guniverseF = (L1 <$> guniverseF) ++ (R1 <$> guniverseF)

instance (GFinite f, GFinite g) => GFinite (f :*: g) where
  guniverseF = (:*:) <$> guniverseF <*> guniverseF

instance Finite a => GFinite (K1 i a) where
  guniverseF = K1 <$> universeF

instance GFinite f => GFinite (M1 i c f) where
  guniverseF = M1 <$> guniverseF

universeFGeneric :: (Generic a, GFinite (Rep a)) => [a]
universeFGeneric = to <$> guniverseF

newtype ViaGUniverse a = ViaGUniverse  a
newtype ViaGFinite   a = ViaGFinite a

instance (Generic a, GUniverse (Rep a)) => Universe (ViaGUniverse a) where
  universe = coerce $ universeGeneric @a

instance (Generic a, GUniverse (Rep a), GFinite (Rep a)) => Finite (ViaGUniverse a) where
  universeF = coerce $ universeFGeneric @a

instance (Generic a, GFinite (Rep a)) => Universe (ViaGFinite a) where
  universe = coerce $ universeFGeneric @a

instance (Generic a, GFinite (Rep a)) => Finite (ViaGFinite a) where
  universeF = coerce $ universeFGeneric @a
