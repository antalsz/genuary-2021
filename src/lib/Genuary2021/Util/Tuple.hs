{-# LANGUAGE TupleSections #-}

module Genuary2021.Util.Tuple (
 -- * Triples
 curry3, uncurry3, fst3, snd3, thd3,
 -- * Preserving/annotating
 preserving, annotating,
 preservingF, annotatingF
) where

curry3 :: ((a,b,c) -> d) -> a -> b -> c -> d
curry3 f x y z = f (x,y,z)
{-# INLINE curry3 #-}

uncurry3 :: (a -> b -> c -> d) -> (a,b,c) -> d
uncurry3 f (x,y,z) = f x y z
{-# INLINE uncurry3 #-}

fst3 :: (a,b,c) -> a
fst3 (a,_,_) = a
{-# INLINE fst3 #-}

snd3 :: (a,b,c) -> b
snd3 (_,b,_) = b
{-# INLINE snd3 #-}

thd3 :: (a,b,c) -> c
thd3 (_,_,c) = c
{-# INLINE thd3 #-}

preserving :: (a -> b) -> a -> (a,b)
preserving f x = (x, f x)

annotating :: (a -> b) -> a -> (b,a)
annotating f x = (f x, x)

preservingF :: Functor f => (a -> f b) -> a -> f (a,b)
preservingF f x = (x,) <$> f x

annotatingF :: Functor f => (a -> f b) -> a -> f (b,a)
annotatingF f x = (,x) <$> f x
