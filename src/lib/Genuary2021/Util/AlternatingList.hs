{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE PatternSynonyms #-}

module Genuary2021.Util.AlternatingList (
  -- * General-purpose alternating list types

  -- | These types were written for January 8, but the code god long enough (so
  -- many instances) and the names qualified enough (please import this module
  -- qualified) that I wanted to pull it out.  Maybe it will be useful in the
  -- future!
  
  List(..), NonEmpty(.., Uncons),

  -- * Basic accessors (primes are for 'NonEmpty')
  head, head', uncons, uncons', mapHead, mapHead',

  -- * Converting to (nonempty) lists
  toEithers, toEithers', toList, toNonEmpty, toListBy, toNonEmptyBy,

  -- * Converting from 'Foldable' types
  group, foldGroup, groupBy, foldGroupBy, groupWith, groupWithBy
) where

import Prelude hiding (head)

import Data.Bifunctor
import Data.Bifoldable
import Data.Bitraversable

import qualified Data.List.NonEmpty as List
  
-- |Unordered possibly-empty alternating lists: @a?(ba)*b?@
data List a b = Empty
              | Start1 (NonEmpty a b)
              | Start2 (NonEmpty b a)
              deriving (Eq, Ord, Show, Read)

-- |Ordered nonempty alternating lists: @a(ba)*b?@
data NonEmpty a b = One a
                  | a :~ NonEmpty b a
                  deriving (Eq, Ord, Show, Read)
infixr 5 :~

-- |An isomorphic representation of 'NonEmpty'
pattern Uncons :: a -> Maybe (NonEmpty b a) -> NonEmpty a b
pattern Uncons a mbas <- (uncons' -> (a, mbas)) where
  Uncons a mbas = maybe (One a) (a :~) mbas
{-# COMPLETE Uncons #-}

instance Bifunctor List where
  bimap _ _ Empty        = Empty
  bimap f g (Start1 abs) = Start1 $ bimap f g abs
  bimap f g (Start2 bas) = Start2 $ bimap g f bas

instance Bifunctor NonEmpty where
  bimap f _ (One a)    = One $ f a
  bimap f g (a :~ bas) = f a :~ bimap g f bas

instance Bifoldable List where
  bifoldMap _ _ Empty        = mempty
  bifoldMap f g (Start1 abs) = bifoldMap f g abs
  bifoldMap f g (Start2 bas) = bifoldMap g f bas

  bifoldr _ _ z Empty        = z
  bifoldr f g z (Start1 abs) = bifoldr f g z abs
  bifoldr f g z (Start2 bas) = bifoldr g f z bas

  bifoldl _ _ z Empty     = z
  bifoldl f g z (Start1 abs) = bifoldl f g z abs
  bifoldl f g z (Start2 bas) = bifoldl g f z bas

instance Bifoldable NonEmpty where
  bifoldMap f _ (One a)    = f a
  bifoldMap f g (a :~ bas) = f a <> bifoldMap g f bas

  bifoldr f _ z (One a)    = f a z
  bifoldr f g z (a :~ bas) = f a $ bifoldr g f z bas

  bifoldl f _ z (One a)    = f z a
  bifoldl f g z (a :~ bas) = bifoldl g f (f z a) bas

instance Bitraversable List where
  bitraverse _ _ Empty        = pure Empty
  bitraverse f g (Start1 abs) = Start1 <$> bitraverse f g abs
  bitraverse f g (Start2 bas) = Start2 <$> bitraverse g f bas

instance Bitraversable NonEmpty where
  bitraverse f _ (One a)    = One  <$> f a
  bitraverse f g (a :~ bas) = (:~) <$> f a <*> bitraverse g f bas

instance Functor     (List     a) where fmap = second
instance Functor     (NonEmpty a) where fmap = second
instance Foldable    (List     a) where foldMap = bifoldMap (const mempty) ; foldr = bifoldr (flip const)
instance Foldable    (NonEmpty a) where foldMap = bifoldMap (const mempty) ; foldr = bifoldr (flip const)
instance Traversable (List     a) where traverse = bitraverse pure
instance Traversable (NonEmpty a) where traverse = bitraverse pure

head :: List a b -> Maybe (Either a b)
head Empty        = Nothing
head (Start1 abs) = Just . Left  $ head' abs
head (Start2 bas) = Just . Right $ head' bas

head' :: NonEmpty a b -> a
head' (One a)  = a
head' (a :~ _) = a

uncons :: List a b -> Maybe (Either a b, List a b)
uncons Empty = Nothing
uncons (Start1 abs) = Just . bimap Left  (maybe Empty Start2) $ uncons' abs
uncons (Start2 bas) = Just . bimap Right (maybe Empty Start1) $ uncons' bas

uncons' :: NonEmpty a b -> (a, Maybe (NonEmpty b a))
uncons' (One a)    = (a, Nothing)
uncons' (a :~ bas) = (a, Just bas)

mapHead :: (a -> a) -> (b -> b) -> List a b -> List a b
mapHead _ _ Empty        = Empty
mapHead f _ (Start1 abs) = Start1 $ mapHead' f abs
mapHead _ g (Start2 bas) = Start2 $ mapHead' g bas

mapHead' :: (a -> a) -> NonEmpty a b -> NonEmpty a b
mapHead' f (One a)    = One $ f a
mapHead' f (a :~ bas) = f a :~ bas

toListBy :: (a -> c) -> (b -> c) -> List a b -> [c]
toListBy f g = bifoldr ((:) . f) ((:) . g) []

toNonEmptyBy :: (a -> c) -> (b -> c) -> NonEmpty a b -> List.NonEmpty c
toNonEmptyBy f g (Uncons a mbas) = f a List.:| foldMap (bifoldr ((:) . g) ((:) . f) []) mbas

toEithers :: List a b -> [Either a b]
toEithers = toListBy Left Right

toEithers' :: NonEmpty a b -> List.NonEmpty (Either a b)
toEithers' = toNonEmptyBy Left Right

toList :: List a a -> [a]
toList = toListBy id id

toNonEmpty :: NonEmpty a a -> List.NonEmpty a
toNonEmpty = toNonEmptyBy id id

groupWithBy :: Foldable f
            => (b -> b -> b) -> (c -> c -> c)
            -> (a -> Either b c)
            -> f a -> List b c
groupWithBy l r f = foldr (step . f) Empty where
  step (Left  x) (Start1 xys) = Start1 $ mapHead' (l x) xys
  step (Right y) (Start2 yxs) = Start2 $ mapHead' (r y) yxs
  step (Left  x) Empty        = Start1 $ One x
  step (Right y) Empty        = Start2 $ One y
  step (Left  x) (Start2 yxs) = Start1 $ x :~ yxs
  step (Right y) (Start1 xys) = Start2 $ y :~ xys

groupWith :: Foldable f
          => (a -> a -> a) -> (b -> b -> b)
          -> f (Either a b) -> List a b
groupWith l r = groupWithBy l r id

groupBy :: Foldable f
        => (a -> Either b c)
        -> f a -> List (List.NonEmpty b) (List.NonEmpty c)
groupBy = foldGroupBy . (bimap pure pure .)
-- Could also use this as the core instead of 'groupWithBy'

group :: Foldable f => f (Either a b) -> List (List.NonEmpty a) (List.NonEmpty b)
group = groupBy id

foldGroupBy :: (Semigroup b, Semigroup c, Foldable f) => (a -> Either b c) -> f a -> List b c
foldGroupBy = groupWithBy (<>) (<>)

foldGroup :: (Semigroup a, Semigroup b, Foldable f) => f (Either a b) -> List a b
foldGroup = groupWith (<>) (<>)
