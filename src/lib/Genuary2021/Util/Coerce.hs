module Genuary2021.Util.Coerce (coerce1Arg, coerce2Args, coerce3Args) where

import Data.Coerce

-- |@coerce@ specialized to convert the argument to a function and leave the
-- result invariant.  Useful for e.g. converting @Wrapped a -> m b@ to @a -> m
-- b@ without @coerce@ getting confused about the @m b@ part.
coerce1Arg :: Coercible a a' => (a -> r) -> (a' -> r)
coerce1Arg = coerce
{-# INLINE coerce1Arg #-}

-- |@coerce@ specialized to convert the arguments to a 2-argument function, and
-- leave the result invariant; line 'coerce1Arg', but one more.
coerce2Args :: (Coercible a a', Coercible b b')
            => (a -> b -> r) -> (a' -> b' -> r)
coerce2Args = coerce
{-# INLINE coerce2Args #-}

-- |@coerce@ specialized to convert the arguments to a 3-argument function, and
-- leave the result invariant; like 'coerce2Args', but one more.
coerce3Args :: (Coercible a a', Coercible b b', Coercible c c')
             => (a -> b -> c -> r) -> (a' -> b' -> c' -> r)
coerce3Args = coerce
{-# INLINE coerce3Args #-}
