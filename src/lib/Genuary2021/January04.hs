{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}

module Genuary2021.January04 where

-- Prompt: Small areas of symmetry.
-- (One day late)

import Data.Traversable
import Control.Monad
import Data.Bitraversable
import Data.Function
import Genuary2021.Util.Function
import Genuary2021.Util.Ord

import Data.Char
import Data.Word

import Numeric.Noise.Perlin

import Data.Random
import Genuary2021.Util.Random
import System.Random.PCG.Class (sysRandom)
import Genuary2021.Util.Random.Source.PCG

import Codec.Picture

import Data.Colour
import Data.Colour.RGBSpace
import Data.Colour.SRGB
import Data.Colour.Names
import HSLuv

import Data.Array.IArray

import Text.Read
import Options.Applicative

--------------------------------------------------------------------------------
-- Utility functions

lerp :: (Num a, Ord a) => a -> a -> a -> a
lerp l h t =  l + (clamp 0 1 t)*(h-l)

--------------------------------------------------------------------------------
-- Image piece 1: grid of colored squares

-- I'd hide the constructor if I were modularizing
-- Invariant: The bounds are ((0,0),(k,k))
newtype ColorTile = ColorTile (Array (Int, Int) (Colour Double))
                  deriving (Eq, Show)

randomSeparatedColorTile :: Int -> RVarT m ColorTile
randomSeparatedColorTile side = do
  let count  = side*side
      hueGap = 360 / fromIntegral count
      
      normalize hue | hue > 360 = hue - 360
                    | otherwise = hue

  base <- uniformT 0 360
  colorTile <$> for [0..count-1] \i -> do
    let hueStop = normalize $ base + hueGap * fromIntegral i
    hue       <- normalT hueStop (hueGap / (fromIntegral count / 3))
    lightness <- clamp 0 100 <$> normalT 65 8.75
    pure . hsluvToColour $ HSLuv (HSLuvHue        hue)
                                 (HSLuvSaturation 100)
                                 (HSLuvLightness  lightness)

randomClusteredColorTile :: Int -> RVarT m ColorTile
randomClusteredColorTile side = do
  let count = side*side
  base <- uniformT 0 360
  colorTile <$> replicateM count do
    hue        <- normalT base (360 / fromIntegral count)
    saturation <- clamp 0 100 <$> normalT 65 8.75
    lightness  <- clamp 0 100 <$> normalT 65 8.75
    pure . hsluvToColour $ HSLuv (HSLuvHue        hue)
                                 (HSLuvSaturation saturation)
                                 (HSLuvLightness  lightness)

colorTile :: [Colour Double] -> ColorTile
colorTile colors = ColorTile
  let side = ceiling . sqrt @Double . fromIntegral $ length colors
  in listArray ((0,0), (side-1, side-1)) $ colors ++ repeat black
{-# INLINABLE colorTile #-}

-- Wraps around
colorAtWrapping :: ColorTile -> Int -> Int -> Colour Double
colorAtWrapping (ColorTile colors) x y =
  let ((_, _), (cmax, _)) = bounds colors
      csize = cmax + 1
  in colors ! (y `rem` csize, x `rem` csize)
     -- Arrays are stored column-major, it's very confusing
{-# INLINE colorAtWrapping #-}

data ColorGrid = ColorGrid { tile    :: !ColorTile
                           , boxSize :: !Int }
               deriving (Eq, Show)

colorGridColor :: ColorGrid -> Int -> Int -> Colour Double
colorGridColor ColorGrid{..} =  colorAtWrapping tile `on` (`quot` boxSize)

--------------------------------------------------------------------------------
-- Image piece 2: Perlin noise cloud

data Cloud = Cloud { cloudPerlinSeed        :: !Int
                   , cloudPerlinOctaves     :: !Int    -- The docs had 5
                   , cloudPerlinScale       :: !Double -- The docs had 0.05
                   , cloudPerlinPersistence :: !Double -- The docs had 0.5
                   , cloudPerlinZPlane      :: !Double -- This is totally arbitrary
                   }
           deriving (Eq, Ord, Show, Read)

defaultCloud :: Int -> Cloud
defaultCloud cloudPerlinSeed = Cloud { cloudPerlinSeed
                                     , cloudPerlinOctaves     = 5
                                     , cloudPerlinScale       = 2.5
                                     , cloudPerlinPersistence = 0.5
                                     , cloudPerlinZPlane      = 0 }

-- Tuned for @x@ and @y@ in [0,1]
cloudIntensity :: Cloud -> Double -> Double -> Double
cloudIntensity Cloud{..} x y =
  let n = noiseValue (perlin cloudPerlinSeed cloudPerlinOctaves cloudPerlinScale cloudPerlinPersistence)
                     (x, y, cloudPerlinZPlane)
  in (n+1)/2 -- Remap to [0,1]

--------------------------------------------------------------------------------
-- Image piece 3: Clear circular windows

data Window = Window { centerX           :: !Int
                     , centerY           :: !Int
                     , transparentRadius :: !Double
                     , shadingRadius     :: !Double }
            deriving (Eq, Ord, Show, Read)

windowOpacity :: Window -> Int -> Int -> Double
windowOpacity Window{..} x y =
  let (^.) = (^) @Int @Int
      distance = sqrt . fromIntegral $ (x - centerX)^.2 + (y - centerY)^.2
      smootherstep d = d*d*d*(d*(6*d - 15) + 10) -- 6d⁵ - 15d⁴ + 10d³, due to Ken Perlin
  in lerp 0 1 . smootherstep . clamp 0 1 $ (distance - transparentRadius)/shadingRadius

type Windows = [Window]

combinedWindowOpacity :: Windows -> Int -> Int -> Double
combinedWindowOpacity windows x y = product [windowOpacity window x y | window <- windows]
  -- As per user287001's answer to Kamil S.'s question "Combining transparency",
  -- on the Graphic Design stack exchange:
  -- <https://graphicdesign.stackexchange.com/a/91018/159546>.  This is exactly
  -- the probability combination formula, 1 - (1-o1)*(1-o2); as my dad pointed
  -- out, that's because it's the probability that light gets through.  But we
  -- want the reverse – we want any transparency to go all the way through,
  -- instead of any opacity to block everything.  So it's just the product!

--------------------------------------------------------------------------------
-- Combined image

data Windowpane = Windowpane { width     :: !Int
                             , height    :: !Int
                             , colorGrid :: !ColorGrid
                             , cloud     :: !Cloud
                             , windows   :: !Windows }
                deriving (Eq, Show)

windowpaneColor :: Windowpane -> Int -> Int -> Colour Double
windowpaneColor Windowpane{..} x y =
  let xf = fromIntegral x / fromIntegral width
      yf = fromIntegral y / fromIntegral height
      
      tileColor  =                    colorGridColor        colorGrid x  y
      cloudColor = (`darken` white) $ cloudIntensity        cloud     xf yf
      opacity    =                    combinedWindowOpacity windows   x  y
  in blend opacity cloudColor tileColor

windowpaneImage :: Windowpane -> Image PixelRGB16
windowpaneImage windowpane =
  generateImage (uncurryRGB PixelRGB16 . toSRGBBounded .: windowpaneColor windowpane)
                (width  windowpane)
                (height windowpane)

defaultWindowpane :: Windowpane
defaultWindowpane = Windowpane { width     = 500
                               , height    = 500
                               , colorGrid = ColorGrid { tile    = colorTile [ red,    orange,   yellow
                                                                             , green,  lime,     blue
                                                                             , indigo, lavender, violet ]
                                                       , boxSize = 50 }
                               , cloud     = defaultCloud 1
                               , windows   = [Window 250 250 50 25] }

-- This was only tested with width = height = 500
randomWindowpane :: Int -> Int -> RVarT m Windowpane
randomWindowpane width height = do
  let boxSize  = 25 -- Static, I think
      gridEdge = 3  -- Static, I think
  
  tile <- choose 0.5 randomSeparatedColorTile randomClusteredColorTile >>= ($ gridEdge)
  
  cloudPerlinSeed <- stdUniformT
  
  windowCount <- uniformT 5 15
  windows     <- replicateM windowCount do
                   centerX <- uniformT 0 $ width-1
                   centerY <- uniformT 0 $ height-1
                   transparentRadius <- normalT 20 7
                   shadingRadius     <- normalT 40 14
                   pure Window{..}
  
  pure Windowpane{ colorGrid = ColorGrid{..}
                 , cloud     = defaultCloud cloudPerlinSeed
                 , .. }

--------------------------------------------------------------------------------
-- CLI

data Seed = Genuary2021Seed
          | ExplicitSeed !Word64 !Word64
          | RandomSeed
          deriving (Eq, Ord, Show, Read)

-- Bright colors
genuary2021Seed :: (Word64, Word64)
genuary2021Seed = (18037070984577959364, 5283061165460049800)

-- Cool colors; mentioned in the docs
runnerUpSeed :: (Word64, Word64)
runnerUpSeed = (11888530686096627650, 13626638712721275790)

getSeed :: Seed -> IO (Word64, Word64)
getSeed Genuary2021Seed            = pure genuary2021Seed
getSeed (ExplicitSeed seed1 seed2) = pure (seed1, seed2)
getSeed RandomSeed                 = (,) <$> sysRandom <*> sysRandom

data Arguments = Arguments { seed       :: !Seed
                           , argWidth   :: !Int
                           , argHeight  :: !Int
                           , outputFile :: !FilePath }
               deriving (Eq, Ord, Show, Read)

argumentsParser :: Parser Arguments
argumentsParser = (genuary2021 <|> subcommands) <*> output where
  subcommands =   commandlike "genuary2021"
                              "Generate the default (500×500) Genuary 2021 windowpane (default)"
                              genuary2021
              <|> commandlike "configured"
                              "Generate a windowpane with a customizable seed, width, and height"
                              configured
  
  commandlike name desc opts = flag' () (long name <> help desc) *> opts
  
  -- Default: Genuary2021 @ 500×500
  genuary2021 = pure $ Arguments Genuary2021Seed 500 500
  -- Otherwise, parse the CLI
  configured  = Arguments
    <$> (   flag' Genuary2021Seed
              (  long "genuary2021-seed"
              <> short 'g'
              <> help "Generate a windowpane using the Genuary 2021 seed" )
        <|> option (uncurry ExplicitSeed <$> eitherReader readSeed)
              (  long "seed"
              <> short 's'
              <> metavar "\"WORD64 WORD64\""
              <> help "Generate a windowpane using a PCG PRNG initialized with the given seed" )
        <|> flag' RandomSeed
              (  long "random"
              <> short 'r'
              <> help "Generate a random windowpane, and print out the seed used" ) )
    <*> dimension "width"  'w'
    <*> dimension "height" 'h'
  
  readSeed = bitraverse readEither readEither . break isSpace
  
  dimension what c =
    let positive = do i <- auto
                      if i > 0
                      then pure i
                      else fail "Must be positive"
    in option positive
         $  long what
         <> short c
         <> metavar (map toUpper what)
         <> help ("The output image " ++ what)
  
  output = strArgument
             (  metavar "FILE"
             <> help "The output PNG file to write to" )

argumentsInfo :: ParserInfo Arguments
argumentsInfo = info
  (argumentsParser <**> helper)
  (fullDesc <> progDesc "Generate a \"windowpane\" image.  \
                        \Without arguments, generates the Genuary 2021 windowpane.  \
                        \This program sometimes generates a grid of very distinct colors \
                        \(as in the Genuary 2021 image), and sometimes generates a grid of similar colors \
                        \(try \"11888530686096627650 13626638712721275790\").")

mainWith :: Arguments -> IO ()
mainWith Arguments{..} = do
  (seed1,seed2) <- getSeed seed
  when (seed == RandomSeed) $
    putStrLn $ "Seed: " ++ show seed1 ++ " " ++ show seed2
  
  gen        <- initialize seed1 seed2
  windowpane <- sampleFrom @RVar gen $ randomWindowpane argWidth argHeight
  writePng outputFile $ windowpaneImage windowpane

main :: IO ()
main = mainWith =<< execParser argumentsInfo
