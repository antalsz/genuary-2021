{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}

module Genuary2021.January12 where

import Genuary2021.Util.Ord

import Data.Random

import Diagrams.Prelude hiding (sample, atLeast)
import Diagrams.Backend.SVG.CmdLine

import Genuary2021.Util.Color
import Genuary2021.Util.Random.Source.PCG

import Genuary2021.Util.NASA.Fireball

drawFireball :: Fireball -> ()
drawFireball _ = ()

-- latitude,      longitude,  altitude,    velocity,  energy,                    impactEnergy,              vel3
-- position of V, width of V, radius of O, size of V, lightness/saturation of V, lightness/saturation of O, ?

-- Plan: path -> stroke -> get borders -> greeble edges with noise -> fill with gradient/s
-- Maybe can skip path -> stroke by just using annulus and… something for the V
-- Compute and V annulus directly from structure + noise
-- Is this gonna be easier with Diagrams or bitmap image generation?

-- Could use date/vel3/everything as the noise seed

data Resolution a = NumSamples  !Word
                  | SampleEvery !a
                  deriving (Eq, Ord, Show, Read)

data Openness = Closed | Open deriving (Eq, Ord, Enum, Bounded, Show, Read)

data Bound a = Bound { openness :: !Openness, bound :: !a }
             deriving (Eq, Ord, Show, Read)

resolve :: (Fractional a, Ord a) => Resolution a -> Bound a -> Bound a -> [a]
resolve resolution (Bound ol l) (Bound oh h) =
  case resolution of
    NumSamples  n ->
      let r = (h - l) / fromIntegral (n + 1 - reduce ol - reduce oh & atLeast 1)
      in [l + r*i | i <- subtract (reduce ol) . fromIntegral <$> [1..n]]
    SampleEvery d ->
      let below = case oh of
                    Closed -> (<=)
                    Open   -> (<)
      in takeWhile (`below` h) . drop (1 - reduce ol) $ iterate (+d) l
  where
    reduce Closed = 1
    reduce Open   = 0

data PathWithNormal a b = PathWithNormal { function, unitNormal :: !(a -> b)
                                         , start, end       :: !(Bound a) }

pwnCircle :: Floating a => a -> PathWithNormal a (Point V2 a)
pwnCircle r = PathWithNormal { function   = (r *^) . circle
                             , unitNormal = circle
                             , start      = Bound Closed 0
                             , end        = Bound Open $ 2*pi }
  where circle θ = p2 (cos θ, sin θ)

cubicGreeble :: (Applicative f, TrailLike t, OrderedField a, Fractional (V t (N t)))
             => Openness
             -> PathWithNormal a (Point (V t) (N t))
             -> (a -> Point (V t) (N t) -> f (N t))
             -> Resolution a
             -> f t
cubicGreeble openness PathWithNormal{..} noise resolution =
  fmap interpolate . traverse noisyPath $ resolve resolution start end
  where 
    noisyPath t = let pt = function   t
                      n  = unitNormal t
                  in noise t pt <&> \s -> pt ^+^ s*^n
    
    interpolate = cubicSpline case openness of
                                Open -> False
                                Closed -> True

cubicGreeble' :: (Applicative f, TrailLike t, OrderedField a, Fractional (V t (N t)))
              => Openness
              -> PathWithNormal a (Point (V t) (N t))
              -> f (N t)
              -> Resolution a
              -> f t
cubicGreeble' openness path noise = cubicGreeble openness path \_ _ -> noise

-- Various things could be parameters here:
--   * The radius
--   * The annulus width
--   * The flame height
--   * The flame exponent
--   * The inner vs. outer ratios
--   * The resolution
-- But I'm visually tuning all of them together.
flamingCircle :: (TypeableFloat n, Distribution Uniform n, Renderable (Path V2 n) b)
              => Colour Double -> Colour Double -> RVarT m (QDiagram b V2 n Any)
flamingCircle coreColor flameColor = do
  let radius         = 1
      width          = 0.05
      flameHeight    = 0.125
      flameExponent  = 20
      resolution     = 500
      flameColorFrac = 3/4
      flameColorBand = 0.1

      circle sign h e r = cubicGreeble' Closed
                                        (pwnCircle $ radius + sign (width/2))
                                        ((sign h*) . (**e) <$> uniformT 0 1)
                                        (NumSamples r)
      
      gradient = mkRadialGradient (mkStops [ (flameColor, 0,                      0)
                                           , (flameColor, 1/3 - flameColorBand/2, 1)
                                           , (coreColor,  1/3,                    1)
                                           , (flameColor, 1/3 + flameColorBand/2, 1)
                                           , (flameColor, 1,                      0) ])
                                  origin  (radius -   flameColorFrac*flameHeight)
                                  origin  (radius + 2*flameColorFrac*flameHeight)
                                  GradPad
      
  outer <- circle id     flameHeight     flameExponent     resolution
  inner <- circle negate (flameHeight/2) (flameExponent*2) (resolution `quot` 2)
  let annulus = outer <> reversePath inner
  pure $ strokePath annulus # lw none # fillTexture gradient

main :: IO ()
main = mainWith @(Diagram B) . bgFrame 0.1 black =<< do
         pcg <- initialize 1337 42
         let c = hsluv 60 1 0.85
         cs <- sampleFrom @RVar pcg $ traverse sequence $ replicate 1 (replicate 1 $ flamingCircle white c)
         pure $ hsep 1 $ map (vsep 1) cs
         -- sampleFrom @RVar pcg $ flamingCircle white c
