{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Genuary2021.January11 where

import Data.Coerce
import Data.Typeable

import Data.Function
import Data.Tuple
import Data.Maybe
import Genuary2021.Util.List
import Control.Applicative

import Data.Set as S

import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine

newtype D8  = D8 Int deriving (Eq, Ord, Show, Read)
newtype D20 = D20 Int deriving (Eq, Ord, Show, Read)

-- Prompt: Use something other than a computer as an autonomous process (or use
-- a non-computer random source).

-- I did #2, and rolled dice as my random source.  I have four sets of acrylic
-- dice, so I assigned an order to them (twilight, rainbow, sunset, twilight
-- (white text)), and then rolled the d8s a bunch of times followed by the d20s
-- a bunch of times and wrote it down.  (Technically, my first batch was 4d8 and
-- 4d20, but that was too much of a hassle to record.)  I decided 64 was a nice
-- round number of samples right before I got there.

-- Generated by rolling 16×(4d8)
d8s :: [D8]
d8s = coerce [2,7,6,3,8,2,5,2,3,5,5,2,3,7,3,7,1,7,7,7,8,2,5,2,8,7,4,5,8,8,8,5,2,2,5,1,7,5,7,7,6,2,1,4,5,8,1,4,7,1,7,3,3,6,2,3,7,8,5,3,1,2,7,7 :: Int]

-- Generated by rolling 16×(4d20)
d20s :: [D20]
d20s = coerce [16,19,12,1,15,7,6,17,10,18,17,13,11,16,4,12,2,17,13,11,15,18,15,2,18,15,7,6,12,12,1,20,6,18,2,5,15,5,12,16,6,9,12,9,18,1,16,19,5,11,6,2,5,20,10,20,10,3,7,4,6,15,16,7 :: Int]

color :: (Ord a, Floating a) => D8 -> Colour a
color (D8 1) = hotpink -- pink, after the original 8-stripe pride flag
color (D8 2) = red
color (D8 3) = orange
color (D8 4) = yellow
color (D8 5) = green
color (D8 6) = deepskyblue -- blue
color (D8 7) = mediumblue -- indigo
color (D8 8) = darkviolet -- violet
color (D8 _) = error "d8 out of range"

newtype Spacing n = Spacing n deriving (Eq, Ord, Show, Read)

shape  :: (TrailLike p, Transformable p, Alignable p, HasOrigin p, V p ~ V2)
       => Spacing (N p) -> D20 -> D20 -> p
shape (Spacing gap) (D20 x) (D20 y) =
  let radius   = 0.5 - gap
      diameter = 1 - 2*gap
      whichHalf i = (i-1) `quot` 10
  in case 2*whichHalf x + whichHalf y of
       0 -> circle radius
       1 -> triangle diameter # centerXY
       2 -> reflectY $ triangle diameter # centerXY
       3 -> square diameter
       _ -> error "d20s out of range"

point :: Num n => D20 -> D20 -> P2 n
point (D20 x) (D20 y) =
  let coord i = fromIntegral $ (i-1) `rem` 10
  in p2 (coord x, coord y)

sketchShape :: (TrailLike d, Transformable d, Alignable d, HasOrigin d, V d ~ V2)
            => Spacing (N d) -> D20 -> D20 -> d
sketchShape gap sp₁ sp₂ = shape gap sp₁ sp₂ # moveTo (point sp₁ sp₂)

drawShape :: (TrailLike d, Transformable d, Alignable d, HasOrigin d, HasStyle d, Typeable (N d), V d ~ V2)
          => Spacing (N d) -> D8 -> D20 -> D20 -> d
drawShape gap c sp₁ sp₂ = sketchShape gap sp₁ sp₂ # lw none # fc (color c)

unitGrid :: (TrailLike d, Monoid d, Typeable (N d), HasStyle d, V d ~ V2) => Int -> d
unitGrid n =
  let coord i = fromIntegral @Int i - 0.5
      low  = coord 0
      high = coord n
      line mk = ((~~) `on` (p2 . mk)) low high
  in mconcat [line (i,) <> line (,i) | i <- coord <$> [0..n]] # lc gray # lw 0.5

drawShapes :: forall d.
              ( TrailLike d, Monoid d
              , Transformable d, Alignable d, HasOrigin d
              , HasStyle d, Typeable (N d)
              , V d ~ V2)
           => Spacing (N d) -> [D8] -> [D20] -> d
drawShapes gap cs sps = mconcat $ zipWith (uncurry . drawShape gap) cs (dedup S.empty $ adjacentPairs sps)
  where
    dedup1 seen loc
      | pt `S.member` seen = Nothing
      | otherwise          = Just $ (loc :) . dedup (S.insert pt seen) 
      where pt = uncurry (point @(N d)) loc

    dedup _ [] = []
    dedup seen (loc:locs) =
      fromMaybe (dedup seen) (dedup1 seen loc <|> dedup1 seen (swap loc)) locs

-- We only use half the d8s – slightly less, actually, since the d20s have three duplicate pairs
drawing :: (TrailLike (QDiagram b V2 n q), Renderable (Path V2 n) b, TypeableFloat n, Monoid q)
        => QDiagram b V2 n q
drawing = bgFrame gap white $ unitGrid 10 <> drawShapes (Spacing gap) d8s d20s
  where gap = 0.0758

main :: IO ()
main = mainWith @(Diagram B) drawing
