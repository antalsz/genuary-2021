{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Genuary2021.January08 where

-- Prompt: Curve only.

import Data.Coerce

import Data.Foldable
import Data.Bitraversable

import Data.Maybe
import Genuary2021.Util.Ord
import Control.Monad.State

import Data.List (isSuffixOf)
import qualified Genuary2021.Util.AlternatingList as Alt

import Diagrams.Prelude hiding (atLeast, option, value, over)
import Diagrams.Path
import qualified Diagrams.Backend.CmdLine as CLI
import Diagrams.Backend.SVG
import Data.Colour
import Genuary2021.Util.Color

import Options.Applicative
import Options.Applicative.Types
import System.Exit

-- This L-system is modified from Wikipedia:
-- <https://en.wikipedia.org/wiki/Sierpi%C5%84ski_curve#Representation_as_Lindenmayer_system>
-- Modifications:
-- ∙ Renamings: F → F₁, G → F₂, + → L, - → R.
-- ∙ Start the axiom with L so that the curve is axis-aligned in the limit.  (R works too.)
-- ∙ End the axiom with R, R so that it curves back to the start nicely.

data SierpińskiAlphabet = F₁ | F₂ | L | R | X
                        deriving (Eq, Ord, Enum, Bounded, Show, Read)

sierpińskiStart :: [SierpińskiAlphabet]
sierpińskiStart = [L, F₁, R, R, X, F₁, R, R, F₁, R, R, X, F₁, R, R]

sierpińskiRule :: SierpińskiAlphabet -> [SierpińskiAlphabet]
sierpińskiRule X = [X, F₁, L, F₂, L, X, F₁, R, R, F₁, R, R, X, F₁, L, F₂, L, X]
sierpińskiRule σ = [σ]

sierpińskis :: [[SierpińskiAlphabet]]
sierpińskis = iterate (concatMap sierpińskiRule) sierpińskiStart

toPattern :: forall n. Floating n => n -> n -> [SierpińskiAlphabet] -> Alt.List n (Angle n)
toPattern d₁ d₂ = coerce . Alt.foldGroup @_ @(Angle n) . mapMaybe \case
                    F₁ -> Just . Left  $ Sum d₁
                    F₂ -> Just . Left  $ Sum d₂
                    L  -> Just . Right $  45@@deg
                    R  -> Just . Right $ -45@@deg
                    X  -> Nothing

toSegments :: RealFloat n => n -> Alt.List n (Angle n) -> [Segment Closed V2 n]
toSegments ε = \case
  Alt.Start1 dθs                 -> go (0@@deg) dθs
  Alt.Start2 (Alt.Uncons θ mdθs) -> foldMap (go θ) mdθs
  Alt.Empty                      -> mempty
  where
    line d = straight <$> vecFwd (d-ε)

    corner θ = do
      cornerPt      <- vecFwd ε
      toDestination <- modify' (^+^ θ) *> vecFwd ε
      pure $ bézier3 cornerPt cornerPt $ cornerPt ^+^ toDestination
    
    vecFwd d = do
      θ <- get
      pure $ (d,θ) @@ r2PolarIso
    
    go θ₀ dθs = toList . Alt.toNonEmpty $ evalState (bitraverse line corner dθs) θ₀

data DrawingParameters n = DrawingParameters { distance1, distance2, cornerRadius :: !n }
                         deriving (Eq, Ord, Show, Read)

sierpińskiWordPattern :: RealFloat n => DrawingParameters n -> [SierpińskiAlphabet] -> [Segment Closed V2 n]
sierpińskiWordPattern DrawingParameters{..} = toSegments cornerRadius . toPattern distance1 distance2

sierpińskiPattern :: RealFloat n => DrawingParameters n -> Int -> [Segment Closed V2 n]
sierpińskiPattern parameters n = sierpińskiWordPattern parameters $ sierpińskis !! atLeast n 0

data ColorParameters = ColorParameters { saturation, lightness :: !Double
                                       , windings              :: !Integer }
                     deriving (Eq, Ord, Show, Read)

colorize :: (TypeableFloat n, Renderable (Path V2 n) b)
         => ColorParameters -> [Path V2 n] -> [QDiagram b V2 n Any]
colorize ColorParameters{..} paths =
  let hueStep = fromIntegral windings * 360 / fromIntegral (length paths)
      color i = hsluv (fromIntegral i * hueStep) saturation lightness
      gradient i pt₁ pt₂ =
        mkLinearGradient (mkStops [(color i, 0, 1), (color $ i+1, 1, 1)])
                         pt₁ pt₂
                         GradPad
      
      draw path i = let (start, stop) = case fold $ pathPoints path of
                                          []        -> (origin, origin)
                                          pts@(_:_) -> (head pts, last pts)
                    in strokePath path # lineTexture (gradient i start stop)
  in zipWith draw paths [0::Int ..]

data SierpińskiParameters n = SierpińskiParameters { drawing    :: !(DrawingParameters n)
                                                   , color      :: !ColorParameters
                                                   , background :: !(Maybe (Colour Double))
                                                   , iterations :: !Int }
                          deriving (Eq, Show, Read)

drawSierpiński :: (TypeableFloat n, Renderable (Path V2 n) b)
               => SierpińskiParameters n -> QDiagram b V2 n Any
drawSierpiński SierpińskiParameters{..}
  = maybe id (bgFrame $ (distance1 drawing + distance2 drawing) / 4) background
  . fold . centerXY
  . colorize color
  . explodeTrail . fromSegments
  $ sierpińskiPattern drawing iterations

sierpińskiParametersParser :: Fractional n => Parser (SierpińskiParameters n)
sierpińskiParametersParser = pure dff

instance (Fractional n, Ord n, Show n, Read n) => CLI.Parseable (SierpińskiParameters n) where
  parser =
    let conditional p errmsg arg = do
          x <- arg
          x <$ unless (p x) (fail errmsg)
        nonnegative, positive, in01 :: (Read a, Num a, Ord a) => ReadM a
        nonnegative = conditional (>= 0)                       "must be nonnegative" auto
        positive    = conditional (> 0)                        "must be positive"    auto
        in01        = conditional ((&&) <$> (>= 0) <*> (<= 1)) "must be in [0,1]"    auto
        readBg      = readerAsk >>= \case
                        "none" -> pure Nothing
                        color  -> Just <$> (   readColourName color
                                           <|> ((`over` black) <$> CLI.readHexColor color) )
        drawing = DrawingParameters <$> option nonnegative
                                          (  short 'd'
                                          <> long "distance-1"
                                          <> metavar "NUM"
                                          <> help "The length of the first kind of straight line"
                                          <> value 1 <> showDefault )
                                    <*> option nonnegative
                                          (  short 'e'
                                          <> long "distance-2"
                                          <> metavar "NUM"
                                          <> help "The length of the second kind of straight line"
                                          <> value 1 <> showDefault )
                                    <*> option nonnegative
                                          (  short 'r'
                                          <> long "corner-radius"
                                          <> metavar "NUM"
                                          <> help "How much to round each corner"
                                          <> value 0.25 <> showDefault )
        color = ColorParameters <$> option in01
                                      (  short 'S'
                                      <> long "saturation"
                                      <> metavar "NUM"
                                      <> help "The saturation of the colors in the HSLuv color space, \
                                              \in the range [0,1]"
                                      <> value 1 <> showDefault )
                                <*> option in01
                                      (  short 'L'
                                      <> long "lightness"
                                      <> metavar "NUM"
                                      <> help "The lightness of the colors in the HSLuv color space, \
                                              \in the range [0,1]"
                                      <> value (2/3) <> showDefault )
                                <*> option positive
                                      (  short 'c'
                                      <> long "color-windings"
                                      <> metavar "INT"
                                      <> help "How many times to loop around the hue wheel"
                                      <> value 4 <> showDefault )
        background = option readBg
                       (  short 'b'
                       <> long "background"
                       <> metavar "COLOR"
                       <> help "The background of the image, or \"none\" for no background"
                       <> value (Just black) <> showDefaultWith (\c -> if c == Just black
                                                                       then "black"
                                                                       else show c))
        iterations = argument nonnegative
                       (  metavar "INT"
                       <> help "How many iterations of the L-system to run"
                       <> value 4 <> showDefault )
    in SierpińskiParameters <$> drawing <*> color <*> background <*> iterations

argumentsInfo :: (Fractional n, Ord n, Show n, Read n)
              => ParserInfo (SierpińskiParameters n, CLI.DiagramOpts)
argumentsInfo = info (((,) <$> CLI.parser <*> CLI.parser) <**> helper')
                     (  fullDesc
                     <> progDesc "Generate a colorful, rounded Sierpiński curve.  \
                                 \(In the limit, this curve is space-filling.)  \
                                 \Without arguments (besides width/height/output), \
                                 \use the default parameters that I used for Genuary 2021 \
                                 \(the width and height were both 500)." )
  where
    -- Copied out of the internals of "Diagrams.Backend.CmdLine"
    helper' = abortOption ShowHelpText $ mconcat
      [ long "help"
      , short '?'
      , help "Show this help text"
      ]


dff :: Fractional n => SierpińskiParameters n
dff = SierpińskiParameters { drawing = DrawingParameters { distance1 = 1
                                                         , distance2 = 1
                                                         , cornerRadius = 0.25 }
                           , color = ColorParameters { saturation = 1
                                                     , lightness  = 2/3
                                                     , windings   = 4 }
                           , background = Just black
                           , iterations = 4 }



main :: IO ()
main = do
  (sierpińskiParameters, diagramOpts) <- execParser argumentsInfo
  unless (".svg" `isSuffixOf` (diagramOpts^.CLI.output)) do
    die "Unknown file type; must specify a \".svg\" file"
  renderSVG (diagramOpts^.CLI.output)
            (fromIntegral <$> mkSizeSpec2D (diagramOpts^.CLI.width) (diagramOpts^.CLI.height))
            (drawSierpiński @Double @B sierpińskiParameters)
