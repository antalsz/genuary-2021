module Genuary2021.January05 where

-- Prompt: Do some code golf! How little code can you write to make something
-- interesting? Share the sketch and its code together if you can.
-- (One day late)

import Codec.Picture
import Options.Applicative

-- 138 characters for the expression, 159 characters for the whole declaration
i::Image PixelRGB8
i=generateImage(\x y->let t%k=round.(*255).abs$t(f x*4*k*pi)*t(f y*pi::Float);f=(/500).fromIntegral in PixelRGB8(sin%1)(cos%1)$sin%2)500 500

-- I choose not to count the writing code, but I tried to be concise
main :: IO ()
main =   flip writePng i
     =<< execParser (info (strArgument (metavar "FILE" <> help "Output file") <**> helper)
                          (fullDesc <> progDesc "Make a pretty picture: *Tetraviolet*"))
