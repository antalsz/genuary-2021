{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module Genuary2021.January01 where

-- Prompt:
-- // TRIPLE NESTED LOOP

import Control.Monad

import Data.Word
import Data.Random

import Data.Colour.SRGB
import Data.Colour.RGBSpace
import Data.Colour.RGBSpace.HSV

import Diagrams.Prelude hiding (clamp, normal)
import Diagrams.Backend.SVG.CmdLine

import Genuary2021.Util.Random.Source.PCG hiding (uniform)
import Genuary2021.Util.Tuple

clamp :: Ord a => a -> a -> a -> a
clamp l h x = l `max` x `min` h

data a `WithStdDev` b = a `WithStdDev` b deriving Show
infix 1 `WithStdDev`

type Polar a = (a, Angle a)

polar :: RealFloat a => Polar a -> P2 a
polar = P . review r2PolarIso

annulusPoint :: Double `WithStdDev` Double -> RVar (Polar Double)
annulusPoint (mᵣ `WithStdDev` σᵣ) = do
  r <- normal mᵣ σᵣ
  θ <- uniform 0 (2*pi)
  pure $ (r, θ @@ rad)

annulusArcPointN :: Double       `WithStdDev` Double
                 -> Angle Double `WithStdDev` Double
                 -> RVar (Polar Double)
annulusArcPointN (mᵣ `WithStdDev` σᵣ) (mₜ `WithStdDev` σₜ) = do
  r <- normal mᵣ        σᵣ
  θ <- normal (mₜ^.rad) σₜ
  pure $ (r, θ @@ rad)

annulusArcPointU :: Double `WithStdDev` Double
                 -> (Angle Double, Angle Double)
                 -> RVar (Polar Double)
annulusArcPointU (mᵣ `WithStdDev` σᵣ) ((θₗ,θₕ)) = do
  r <- normal mᵣ σᵣ
  θ <- uniform (θₗ^.rad) (θₕ^.rad)
  pure $ (r, θ @@ rad)

closedLine :: TrailLike c => [Point (V c) (N c)] -> c
closedLine = trailLike . mapLoc (Trail . closeLine) . fromVertices

sampleFromPCG :: FrozenGen -> RVar a -> a
sampleFromPCG pcg x = fst $ withFrozen pcg \gen -> sampleFrom gen x

data TriangleOptions = TriangleOptions { tPoints       :: !Int
                                       , tRadius       :: !Double
                                       , tRadiusStdDev :: !Double
                                       , tAngleStdDev  :: !Double
                                       , tSeed         :: !(Word64, Word64) }
                     deriving (Eq, Ord, Show, Read)

drawTrianglesBetween :: ( Foldable f1, Foldable f2, Foldable f3
                        , TrailLike a, Monoid a, HasStyle a, V a ~ V2, N a ~ Double)
                     => f1 (Polar Double) -> f2 (Polar Double) -> f3 (Polar Double) -> a
drawTrianglesBetween ps₁ ps₂ ps₃ =
  flip foldMap ps₁ \p₁ ->
    flip foldMap ps₂ \p₂ ->
      flip foldMap ps₃ \p₃ ->
        mconcat [ p₁ ~~~ p₂
                , p₂ ~~~ p₃
                , p₃ ~~~ p₁ ]
  where
    p₁ ~~~ p₂ = polar p₁ ~~ polar p₂ # lineTexture (gradient p₁ p₂)
    
    color (r, θ) = uncurryRGB sRGB $ hsv (θ^.deg) (clamp 0 1 r) 1
    gradient p₁ p₂ = mkLinearGradient (mkStops [(color p₁, 0, 1), (color p₂, 1, 1)])
                                      (polar p₁) (polar p₂)
                                      GradPad

generateTrianglePoints :: TriangleOptions -> ([Polar Double], [Polar Double], [Polar Double])
generateTrianglePoints TriangleOptions{..} =
  sampleFromPCG (uncurry initFrozen tSeed) $
    (,,) <$> points (0/3)
         <*> points (1/3)
         <*> points (2/3)
  where
    point t = annulusArcPointN (tRadius `WithStdDev` tRadiusStdDev)
                               (t @@ turn `WithStdDev` tAngleStdDev)
    points  = replicateM tPoints . point

drawTriangles :: ( TrailLike (QDiagram b V2 Double q)
                 , Renderable (Path V2 Double) b
                 , Monoid q )
              => TriangleOptions -> QDiagram b V2 Double q
drawTriangles topts =
  let points@(ps₁,ps₂,ps₃) = generateTrianglePoints topts
      radius = maximum . map (abs . fst) $ ps₁ ++ ps₂ ++ ps₃
      
      triangle   = uncurry3 drawTrianglesBetween points
      background = circle (radius*1.05) # lw none # fc black
  in (triangle <> background) # pad 1.02

mainTriOpts :: TriangleOptions
mainTriOpts = TriangleOptions { tPoints       = 3
                              , tRadius       = 1
                              , tRadiusStdDev = 1/5
                              , tAngleStdDev  = 1/5
                              , tSeed         = (11669001639861402080, 3448740503755567839) }

main :: IO ()
main = mainWith $ drawTriangles @B @Any mainTriOpts
